package makao.model.card;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import makao.model.card.Card;

@SuppressWarnings("serial")
public class CardImagex extends JPanel
{
	private BufferedImage cardImage;
	private Card card;
	private int index;
	
	private boolean cardInCombo;
	
	public CardImagex(Card _card, boolean clickableCard, Color areaColor)
	{
		try {
			cardImage = ImageIO.read(new File("C:\\Users\\jffs\\Documents\\workspaces\\eclipse\\MakaoCardGame\\cards\\" + _card.getCardImageFileName()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		setSize(cardImage.getWidth(), cardImage.getHeight());
		setPreferredSize(new Dimension(cardImage.getWidth(), cardImage.getHeight()));
		setBorder(null);
		if (clickableCard) {
			setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		}
		setBackground(areaColor);
		card = _card;
		index = 0;
		cardInCombo = false;
	}
	
	public void setIndex(int cardIndex)
	{
		index = cardIndex;
	}
	
	public void setInsideCardCombo()
	{
		cardInCombo = true;
	}
	
	public boolean isInsideCardCombo()
	{
		return cardInCombo;
	}
	
	public int getIndex()
	{
		return index;
	}
	
	public Card getCard()
	{
		return card;
	}
	
	public void setCard(Card _card)
	{
		card = _card;
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(cardImage, 0, 0, cardImage.getWidth(), cardImage.getHeight(), this);
	}
}
