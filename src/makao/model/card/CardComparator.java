package makao.model.card;

public class CardComparator
{
	public static CardCompareResult compareCards(Card c1, Card c2)
	{
		boolean colorMatch, figureMatch;
		
		colorMatch = c1.getColor() == c2.getColor();
		figureMatch = c1.getFigure() == c2.getFigure();
		
		if (colorMatch && figureMatch) {
			return CardCompareResult.COMPARE_RESULT_IDENTICAL;
		}
		
		if (colorMatch && !figureMatch) {
			return CardCompareResult.COMPARE_RESULT_COLOR_MATCH;
		}
		
		if (!colorMatch && figureMatch) {
			return CardCompareResult.COMPARE_RESULT_FIGURE_MATCH;
		}
		
		return CardCompareResult.COMPARE_RESULT_NONE;
	}
}
