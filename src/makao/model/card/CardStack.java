package makao.model.card;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class CardStack
{
	private ArrayList<Card> cards = new ArrayList<Card>();
	
	public CardStack()
	{
		
	}
	
	public Card getCard()
	{
		Card outCard = null;
		
		if (cards.size() > 0) {
			outCard = Card.copy(cards.remove(cards.size() - 1));
		}
		
		return outCard;
	}
	
	public void newCardPack()
	{
		cards = new ArrayList<Card>(Arrays.asList(Card.cards));
		Collections.shuffle(cards);
	}
}
