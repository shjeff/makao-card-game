package makao.model.card;

import java.util.Random;

enum CardType
{
	NORMAL,
	JACK,
	ACE,
	QUEEN,
	QUEUE,
	JOKER,
	BATTLE,
}

public class CardInfiniteWaist
{
	private double chanceNormal;
	private double chanceJack;
	private double chanceAce;
	private double chanceQueue;
	private double chanceQueen;
	private double chanceBattle;
	private double chanceJoker;
	
	private double valueNormal;
	private double valueJack;
	private double valueAce;
	private double valueQueue;
	private double valueQueen;
	private double valueBattle;
	private double valueJoker;
	
	private boolean randomizeNormal;
	private boolean randomizeJack;
	private boolean randomizeAce;
	private boolean randomizeQueue;
	private boolean randomizeQueen;
	private boolean randomizeBattle;
	private boolean randomizeJoker;
	
	private Random random = new Random();
	
	public CardInfiniteWaist()
	{
		
	}
	
	// 1- najwieksze pstwo
	// 0- najmniejsze pstwo
	public void setChance(double _chanceNormal, double _chanceJack, double _chanceAce, double _chanceQueue, double _chanceQueen, double _chanceBattle, double _chanceJoker)
	{
		chanceNormal = _chanceNormal;
		chanceJack = _chanceJack;
		chanceAce = _chanceAce;
		chanceQueue = _chanceQueue;
		chanceQueen = _chanceQueen;
		chanceBattle = _chanceBattle;
		chanceJoker = _chanceJoker;
		
		valueNormal = 0;	
		valueJack = 0;	
		valueAce = 0;	
		valueQueue = 0;	
		valueQueen = 0;	
		valueBattle = 0;	
		valueJoker = 0;	
	}
	
	public void setChanceNormal(double _chanceNormal)
	{
		chanceNormal = _chanceNormal;
	}
	
	public void setChanceJack(double _chanceJack)
	{
		chanceJack = _chanceJack;
	}
	
	public void setChanceAce(double _chanceAce)
	{
		chanceAce = _chanceAce;
	}
	
	public void setChanceQueue(double _chanceQueue)
	{
		chanceQueue = _chanceQueue;
	}
	
	public void setChanceQueen(double _chanceQueen)
	{
		chanceQueen = _chanceQueen;
	}
	
	public void setChanceBattle(double _chanceBattle)
	{
		chanceBattle = _chanceBattle;
	}
	
	public void setChanceJoker(double _chanceJoker)
	{
		chanceJoker = _chanceJoker;
	}
	
	private Card randomJoker()
	{
		String[] jokerColors = new String[] { "Red", "Black", };
		int selectedColorIndex = random.nextInt(2);
		return Card.getCardFromParams(Card.getColorEnum(jokerColors[selectedColorIndex]), CardFigure.CARD_FIGURE_JOKER);
	}
	
	private Card randomBattle()
	{
		String[] battleFigures = new String[] { "2", "3", "King" };
		int selectedFigureIndex = random.nextInt(3);
		int selectedColorIndex;
		String color = "", figure = "";
		
		if (selectedFigureIndex == 0 || selectedFigureIndex == 1) {
			String[] battleColors = new String[] { "Club", "Spade", "Diamond", "Heart" };
			selectedColorIndex = random.nextInt(4);
			
			figure = battleFigures[selectedFigureIndex];
			color = battleColors[selectedColorIndex];
		} else if (selectedFigureIndex == 2) {
			String[] kingColors = new String[] { "Spade", "Heart", };
			selectedColorIndex = random.nextInt(2);
			
			figure = battleFigures[selectedFigureIndex];
			color = kingColors[selectedColorIndex];
		}
		
		return Card.getCardFromParams(Card.getColorEnum(color), Card.getFigureEnum(figure));
	}
	
	private Card randomQueen()
	{
		String[] queenColors = new String[] { "Club", "Spade", "Diamond", "Heart" };
		int selectedColor = random.nextInt(4);
		return Card.getCardFromParams(Card.getColorEnum(queenColors[selectedColor]), CardFigure.CARD_FIGURE_QUEEN);
	}
	
	private Card randomQueue()
	{
		String[] queueColors = new String[] { "Club", "Spade", "Diamond", "Heart" };
		int selectedColor = random.nextInt(4);
		return Card.getCardFromParams(Card.getColorEnum(queueColors[selectedColor]), CardFigure.CARD_FIGURE_4);
	}
	
	private Card randomAce()
	{
		String[] aceColors = new String[] { "Club", "Spade", "Diamond", "Heart" };
		int selectedColor = random.nextInt(4);
		return Card.getCardFromParams(Card.getColorEnum(aceColors[selectedColor]), CardFigure.CARD_FIGURE_ACE);
	}
	
	private Card randomJack()
	{
		String[] jackColors = new String[] { "Club", "Spade", "Diamond", "Heart" };
		int selectedColor = random.nextInt(4);
		return Card.getCardFromParams(Card.getColorEnum(jackColors[selectedColor]), CardFigure.CARD_FIGURE_JACK);
	}
	
	private Card randomNormal()
	{
		String[] normalFigures = new String[] { "5", "6", "7", "8", "9", "10", "King" };
		int selectedFigure = random.nextInt(7);
		String[] randomColors;
		if (selectedFigure == 6) {
			randomColors = new String[] { "Club", "Diamond", };
		} else {
			randomColors = new String[] { "Club", "Spade", "Diamond", "Heart" };
		}
		
		int selectedColor = random.nextInt(randomColors.length);
		return Card.getCardFromParams(Card.getColorEnum(randomColors[selectedColor]), Card.getFigureEnum(normalFigures[selectedFigure]));
	}
	
	private void iniRandomizeLocks()
	{
		// w zaleznosci od p-stwa zaloz blokade na losowaniu wartosci dla danego typu karty
		randomizeNormal = (chanceNormal > 0);
		randomizeJack = (chanceJack > 0);
		randomizeAce = (chanceAce > 0);
		randomizeQueue = (chanceQueue > 0);
		randomizeQueen = (chanceQueen > 0);
		randomizeBattle = (chanceBattle > 0);
		randomizeJoker = (chanceJoker > 0);
	}
	
	private CardType getSelectedCardType()
	{
		if (randomizeNormal) {
			if (valueNormal >= (1 - chanceNormal)) {
				return CardType.NORMAL;
			}
		}
		
		if (randomizeJack) {
			if (valueJack >= (1 - chanceJack)) {
				return CardType.JACK;
			}
		}
		
		if (randomizeAce) {
			if (valueAce >= (1 - chanceAce)) {
				return CardType.ACE;
			}
		}
		
		if (randomizeQueue) {
			if (valueQueue >= (1 - chanceQueue)) {
				return CardType.QUEUE;
			}
		}
		
		if (randomizeQueen) {
			if (valueQueen >= (1 - chanceQueen)) {
				return CardType.QUEEN;
			}
		}
		
		if (randomizeBattle) {
			if (valueBattle >= (1 - chanceBattle)) {
				return CardType.BATTLE;
			}
		}
		
		if (randomizeJoker) {
			if (valueJoker >= (1 - chanceJoker)) {
				return CardType.JOKER;
			}
		}
		
		return null;
	}
	
	private int numPassedCardTypes()
	{
		int passedTypes = 0;
		
		if (randomizeNormal) {
			if (valueNormal >= (1 - chanceNormal)) {
				passedTypes++;
			}
		}
		
		if (randomizeJack) {
			if (valueJack >= (1 - chanceJack)) {
				passedTypes++;
			}
		}
		
		if (randomizeAce) {
			if (valueAce >= (1 - chanceAce)) {
				passedTypes++;
			}
		}
		
		if (randomizeQueue) {
			if (valueQueue >= (1 - chanceQueue)) {
				passedTypes++;
			}
		}
		
		if (randomizeQueen) {
			if (valueQueen >= (1 - chanceQueen)) {
				passedTypes++;
			}
		}
		
		if (randomizeBattle) {
			if (valueBattle >= (1 - chanceBattle)) {
				passedTypes++;
			}
		}
		
		if (randomizeJoker) {
			if (valueJoker >= (1 - chanceJoker)) {
				passedTypes++;
			}
		}
		
		return passedTypes; 
	}
	
	private void lockUnPassed()
	{
		if (randomizeNormal) {
			if (valueNormal < (1 - chanceNormal)) {
				randomizeNormal = false;
			}
		}
		
		if (randomizeJack) {
			if (valueJack < (1 - chanceJack)) {
				randomizeJack = false;
			}
		}
		
		if (randomizeAce) {
			if (valueAce < (1 - chanceAce)) {
				randomizeAce = false;
			}
		}
		
		if (randomizeQueue) {
			if (valueQueue < (1 - chanceQueue)) {
				randomizeQueue = false;
			}
		}
		
		if (randomizeQueen) {
			if (valueQueen < (1 - chanceQueen)) {
				randomizeQueen = false;
			}
		}
		
		if (randomizeBattle) {
			if (valueBattle < (1 - chanceBattle)) {
				randomizeBattle = false;
			}
		}
		
		if (randomizeJoker) {
			if (valueJoker < (1 - chanceJoker)) {
				randomizeJoker = false;
			}
		}
	}
	
	private boolean randomizeChanceOk()
	{
		if (randomizeNormal) {
			valueNormal = random.nextDouble();	
		}
		
		if (randomizeJack) {
			valueJack = random.nextDouble();	
		}
		
		if (randomizeAce) {
			valueAce = random.nextDouble();	
		}
		
		if (randomizeQueue) {
			valueQueue = random.nextDouble();	
		}
		
		if (randomizeQueen) {
			valueQueen = random.nextDouble();	
		}
		
		if (randomizeBattle) {
			valueBattle = random.nextDouble();	
		}
		
		if (randomizeJoker) {
			valueJoker = random.nextDouble();	
		}
		
		int numPassedTypes = numPassedCardTypes();
		if (numPassedTypes == 0) {
			return false; // jezeli nic nie przeszlo losuj jeszcze raz
		}
		
		if (numPassedTypes == 1) {
			return true;
		}
		
		// jezeli przeszlo wiecej
		// zablokuj te ktore nie przeszly
		lockUnPassed();
		
		// i losuj jeszcze raz
		return false;
	}
	
	// funkcja zwraca wyolosowana karte
	public Card getCard()
	{
		iniRandomizeLocks();
		Card card = null;
		
		// losuj tak dlugo az pozostanie jeden typ karty
		while (!randomizeChanceOk()) {	}
		
		CardType selectedType = getSelectedCardType();
		
		switch (selectedType) {
			case JACK:
				card = randomJack();
				break;
				
			case ACE:
				card = randomAce();
				break;
				
			case QUEUE:
				card = randomQueue();
				break;
				
			case QUEEN:
				card = randomQueen();
				break;
				
			case BATTLE:
				card = randomBattle();
				break;
				
			case JOKER:
				card = randomJoker();
				break;
		
			default:
			case NORMAL:
				card = randomNormal();
				break;
		}
		
		return card;
	}
}
