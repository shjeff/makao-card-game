package makao.model.card;

import makao.model.card.CardColor;
import makao.model.card.CardFigure;

public class Card
{
	// biezace parametry karty
	private CardColor cardColor;
	private CardFigure cardFigure;
	private String cardImageFileName;
	
	// kopia oryginalnych parametrow
	private CardColor originalColor;
	private CardFigure originalFigure;
	private String originalImage;
	
	private Card jokerPreference;
	
	private boolean exchanged;
	
	private int cardPoints;
	
	public static Card getCardFromParams(CardColor _cardColor, CardFigure _cardFigure)
	{
		for (int i = 0; i < cards.length; i++) {
			if (cards[i].getColor() == _cardColor && cards[i].getFigure() == _cardFigure) {
				return Card.copy(cards[i]);
			}
		}
		
		return null;
	}
	
	public static Card copy(Card _card)
	{
		return new Card(_card.getColor(), _card.getFigure(), _card.getCardImageFileName());
	}
	
	public Card(CardColor _cardColor, CardFigure _cardFigure, String _cardImageFileName)
	{
		cardColor = _cardColor;
		cardFigure = _cardFigure;
		cardImageFileName = _cardImageFileName;
		
		originalColor = _cardColor;
		originalFigure = _cardFigure;
		originalImage = _cardImageFileName;
		
		cardPoints = 0;
		exchanged = false;
		jokerPreference = null;
		
		setBattlePoints();
	}
	
	public void exchange()
	{
		if (jokerPreference == null) {
			return;
		}
		
		if (!exchanged) {
			cardColor = jokerPreference.getColor();
			cardFigure = jokerPreference.getFigure();
			cardImageFileName = jokerPreference.getCardImageFileName();
		} else {
			cardColor = originalColor;
			cardFigure = originalFigure;
			cardImageFileName = originalImage;
		}
		
		exchanged = !exchanged;
		
		setBattlePoints();
	}
	
	public boolean isExchanged()
	{
		return exchanged;
	}
	
	public void setJokerPreference(Card _jokerPreference)
	{
		jokerPreference = _jokerPreference;
	}
	
	public Card getJokerPreference()
	{
		return jokerPreference;
	}
	
	public void clearJokerPreference()
	{
		if (exchanged) {
			exchange();
		}
		jokerPreference = null;	
	}
	
	public void setAceColor(CardColor _color)
	{
		if (isAce()) {
			cardColor = _color;
		}
	}
	
	public String getCardImageFileName()
	{
		return cardImageFileName;
	}
	
	private void setBattlePoints()
	{
		if (isFightCard()) {
			switch (cardFigure)
			{
			case CARD_FIGURE_KING:
				cardPoints = 5;
				break;
				
			case CARD_FIGURE_2:
				cardPoints = 2;
				break;
				
			case CARD_FIGURE_3:
				cardPoints = 3;
				break;
				
			default:
				cardPoints = 0;
				break;
			}
		}
	}
	
	public boolean isSpecial()
	{
		return isFightCard() || isPause() || isJack() || isJoker() || isAce(); // uwaga !!!!!! DAMA wyrzucona
	}
	
	public CardColor getColor()
	{
		return cardColor;
	}
	
	public CardFigure getFigure()
	{
		return cardFigure;
	}
	
	public int getCardPoints()
	{
		return cardPoints;
	}
	
	// isValorous() / isBattle() czy jest waleczna
	public boolean isFightCard()
	{
		if (cardFigure == CardFigure.CARD_FIGURE_2) {
			return true;
		}
		
		if (cardFigure == CardFigure.CARD_FIGURE_3) {
			return true;
		}
		
		if (cardFigure == CardFigure.CARD_FIGURE_KING) {
			if (cardColor == CardColor.CARD_COLOR_SPADE || cardColor == CardColor.CARD_COLOR_HEART) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean isQueen()
	{
		return cardFigure == CardFigure.CARD_FIGURE_QUEEN;
	}
	
	public boolean isJack()
	{
		return cardFigure == CardFigure.CARD_FIGURE_JACK;
	}
	
	public boolean isJoker()
	{
		return cardFigure == CardFigure.CARD_FIGURE_JOKER;
	}
	
	public boolean isAce()
	{
		return cardFigure == CardFigure.CARD_FIGURE_ACE;
	}
	
	public boolean isPause()
	{
		return cardFigure == CardFigure.CARD_FIGURE_4;
	}
	
	@Override
	public String toString()
	{
		return Card.getColorDesc(cardColor) + ":" + Card.getFigureDesc(cardFigure);
	}
	
	@Override
	public boolean equals(Object _object)
	{
		if (_object instanceof Card) {
			Card card = (Card)_object;
			return (cardFigure == card.getFigure()) && (cardColor == card.getColor());
		}
		return false;
	}
	
	public static Card CARD_JOKER_RED = new Card(CardColor.CARD_COLOR_RED, CardFigure.CARD_FIGURE_JOKER, "cards\\jr.gif");
	public static Card CARD_JOKER_BLACK = new Card(CardColor.CARD_COLOR_BLACK, CardFigure.CARD_FIGURE_JOKER, "cards\\jb.gif");
	
	public static Card CARD_2_CLUB = new Card(CardColor.CARD_COLOR_CLUB, CardFigure.CARD_FIGURE_2, "cards\\c2.gif");
	public static Card CARD_2_SPADE = new Card(CardColor.CARD_COLOR_SPADE, CardFigure.CARD_FIGURE_2, "cards\\s2.gif");
	public static Card CARD_2_DIAMOND = new Card(CardColor.CARD_COLOR_DIAMOND, CardFigure.CARD_FIGURE_2, "cards\\d2.gif");
	public static Card CARD_2_HEART = new Card(CardColor.CARD_COLOR_HEART, CardFigure.CARD_FIGURE_2, "cards\\h2.gif");

	public static Card CARD_3_CLUB = new Card(CardColor.CARD_COLOR_CLUB, CardFigure.CARD_FIGURE_3, "cards\\c3.gif");
	public static Card CARD_3_SPADE = new Card(CardColor.CARD_COLOR_SPADE, CardFigure.CARD_FIGURE_3, "cards\\s3.gif");
	public static Card CARD_3_DIAMOND = new Card(CardColor.CARD_COLOR_DIAMOND, CardFigure.CARD_FIGURE_3, "cards\\d3.gif");
	public static Card CARD_3_HEART = new Card(CardColor.CARD_COLOR_HEART, CardFigure.CARD_FIGURE_3, "cards\\h3.gif");

	public static Card CARD_4_CLUB = new Card(CardColor.CARD_COLOR_CLUB, CardFigure.CARD_FIGURE_4, "cards\\c4.gif");
	public static Card CARD_4_SPADE = new Card(CardColor.CARD_COLOR_SPADE, CardFigure.CARD_FIGURE_4, "cards\\s4.gif");
	public static Card CARD_4_DIAMOND = new Card(CardColor.CARD_COLOR_DIAMOND, CardFigure.CARD_FIGURE_4, "cards\\d4.gif");
	public static Card CARD_4_HEART = new Card(CardColor.CARD_COLOR_HEART, CardFigure.CARD_FIGURE_4, "cards\\h4.gif");

	public static Card CARD_5_CLUB = new Card(CardColor.CARD_COLOR_CLUB, CardFigure.CARD_FIGURE_5, "cards\\c5.gif");
	public static Card CARD_5_SPADE = new Card(CardColor.CARD_COLOR_SPADE, CardFigure.CARD_FIGURE_5, "cards\\s5.gif");
	public static Card CARD_5_DIAMOND = new Card(CardColor.CARD_COLOR_DIAMOND, CardFigure.CARD_FIGURE_5, "cards\\d5.gif");
	public static Card CARD_5_HEART = new Card(CardColor.CARD_COLOR_HEART, CardFigure.CARD_FIGURE_5, "cards\\h5.gif");

	public static Card CARD_6_CLUB = new Card(CardColor.CARD_COLOR_CLUB, CardFigure.CARD_FIGURE_6, "cards\\c6.gif");
	public static Card CARD_6_SPADE = new Card(CardColor.CARD_COLOR_SPADE, CardFigure.CARD_FIGURE_6, "cards\\s6.gif");
	public static Card CARD_6_DIAMOND = new Card(CardColor.CARD_COLOR_DIAMOND, CardFigure.CARD_FIGURE_6, "cards\\d6.gif");
	public static Card CARD_6_HEART = new Card(CardColor.CARD_COLOR_HEART, CardFigure.CARD_FIGURE_6, "cards\\h6.gif");

	public static Card CARD_7_CLUB = new Card(CardColor.CARD_COLOR_CLUB, CardFigure.CARD_FIGURE_7, "cards\\c7.gif");
	public static Card CARD_7_SPADE = new Card(CardColor.CARD_COLOR_SPADE, CardFigure.CARD_FIGURE_7, "cards\\s7.gif");
	public static Card CARD_7_DIAMOND = new Card(CardColor.CARD_COLOR_DIAMOND, CardFigure.CARD_FIGURE_7, "cards\\d7.gif");
	public static Card CARD_7_HEART = new Card(CardColor.CARD_COLOR_HEART, CardFigure.CARD_FIGURE_7, "cards\\h7.gif");

	public static Card CARD_8_CLUB = new Card(CardColor.CARD_COLOR_CLUB, CardFigure.CARD_FIGURE_8, "cards\\c8.gif");
	public static Card CARD_8_SPADE = new Card(CardColor.CARD_COLOR_SPADE, CardFigure.CARD_FIGURE_8, "cards\\s8.gif");
	public static Card CARD_8_DIAMOND = new Card(CardColor.CARD_COLOR_DIAMOND, CardFigure.CARD_FIGURE_8, "cards\\d8.gif");
	public static Card CARD_8_HEART = new Card(CardColor.CARD_COLOR_HEART, CardFigure.CARD_FIGURE_8, "cards\\h8.gif");

	public static Card CARD_9_CLUB = new Card(CardColor.CARD_COLOR_CLUB, CardFigure.CARD_FIGURE_9, "cards\\c9.gif");
	public static Card CARD_9_SPADE = new Card(CardColor.CARD_COLOR_SPADE, CardFigure.CARD_FIGURE_9, "cards\\s9.gif");
	public static Card CARD_9_DIAMOND = new Card(CardColor.CARD_COLOR_DIAMOND, CardFigure.CARD_FIGURE_9, "cards\\d9.gif");
	public static Card CARD_9_HEART = new Card(CardColor.CARD_COLOR_HEART, CardFigure.CARD_FIGURE_9, "cards\\h9.gif");

	public static Card CARD_10_CLUB = new Card(CardColor.CARD_COLOR_CLUB, CardFigure.CARD_FIGURE_10, "cards\\c10.gif");
	public static Card CARD_10_SPADE = new Card(CardColor.CARD_COLOR_SPADE, CardFigure.CARD_FIGURE_10, "cards\\s10.gif");
	public static Card CARD_10_DIAMOND = new Card(CardColor.CARD_COLOR_DIAMOND, CardFigure.CARD_FIGURE_10, "cards\\d10.gif");
	public static Card CARD_10_HEART = new Card(CardColor.CARD_COLOR_HEART, CardFigure.CARD_FIGURE_10, "cards\\h10.gif");
	
	public static Card CARD_JACK_CLUB = new Card(CardColor.CARD_COLOR_CLUB, CardFigure.CARD_FIGURE_JACK, "cards\\cj.gif");
	public static Card CARD_JACK_SPADE = new Card(CardColor.CARD_COLOR_SPADE, CardFigure.CARD_FIGURE_JACK, "cards\\sj.gif");
	public static Card CARD_JACK_DIAMOND = new Card(CardColor.CARD_COLOR_DIAMOND, CardFigure.CARD_FIGURE_JACK, "cards\\dj.gif");
	public static Card CARD_JACK_HEART = new Card(CardColor.CARD_COLOR_HEART, CardFigure.CARD_FIGURE_JACK, "cards\\hj.gif");

	public static Card CARD_QUEEN_CLUB = new Card(CardColor.CARD_COLOR_CLUB, CardFigure.CARD_FIGURE_QUEEN, "cards\\cq.gif");
	public static Card CARD_QUEEN_SPADE = new Card(CardColor.CARD_COLOR_SPADE, CardFigure.CARD_FIGURE_QUEEN, "cards\\sq.gif");
	public static Card CARD_QUEEN_DIAMOND = new Card(CardColor.CARD_COLOR_DIAMOND, CardFigure.CARD_FIGURE_QUEEN, "cards\\dq.gif");
	public static Card CARD_QUEEN_HEART = new Card(CardColor.CARD_COLOR_HEART, CardFigure.CARD_FIGURE_QUEEN, "cards\\hq.gif");
	
	public static Card CARD_KING_CLUB = new Card(CardColor.CARD_COLOR_CLUB, CardFigure.CARD_FIGURE_KING, "cards\\ck.gif");
	public static Card CARD_KING_SPADE = new Card(CardColor.CARD_COLOR_SPADE, CardFigure.CARD_FIGURE_KING, "cards\\sk.gif");
	public static Card CARD_KING_DIAMOND = new Card(CardColor.CARD_COLOR_DIAMOND, CardFigure.CARD_FIGURE_KING, "cards\\dk.gif");
	public static Card CARD_KING_HEART = new Card(CardColor.CARD_COLOR_HEART, CardFigure.CARD_FIGURE_KING, "cards\\hk.gif");
	
	public static Card CARD_ACE_CLUB = new Card(CardColor.CARD_COLOR_CLUB, CardFigure.CARD_FIGURE_ACE, "cards\\ca.gif");
	public static Card CARD_ACE_SPADE = new Card(CardColor.CARD_COLOR_SPADE, CardFigure.CARD_FIGURE_ACE, "cards\\sa.gif");
	public static Card CARD_ACE_DIAMOND = new Card(CardColor.CARD_COLOR_DIAMOND, CardFigure.CARD_FIGURE_ACE, "cards\\da.gif");
	public static Card CARD_ACE_HEART = new Card(CardColor.CARD_COLOR_HEART, CardFigure.CARD_FIGURE_ACE, "cards\\ha.gif");
	
	public static Card CARD_HIDDEN_RED = new Card(CardColor.CARD_COLOR_NONE, CardFigure.CARD_FIGURE_NONE, "cards\\bg\\b2fv.gif");
	public static Card CARD_HIDDEN_BLUE = new Card(CardColor.CARD_COLOR_NONE, CardFigure.CARD_FIGURE_NONE, "cards\\bg\\b1fv.gif");
	
	public static Card[] cards = {
		CARD_JOKER_RED,    CARD_JOKER_BLACK,
		CARD_2_CLUB,       CARD_2_SPADE,       CARD_2_DIAMOND,       CARD_2_HEART,
		CARD_3_CLUB,       CARD_3_SPADE,       CARD_3_DIAMOND,       CARD_3_HEART,
		CARD_4_CLUB,       CARD_4_SPADE,       CARD_4_DIAMOND,       CARD_4_HEART,
		CARD_5_CLUB,       CARD_5_SPADE,       CARD_5_DIAMOND,       CARD_5_HEART,
		CARD_6_CLUB,       CARD_6_SPADE,       CARD_6_DIAMOND,       CARD_6_HEART,
		CARD_7_CLUB,       CARD_7_SPADE,       CARD_7_DIAMOND,       CARD_7_HEART,
		CARD_8_CLUB,       CARD_8_SPADE,       CARD_8_DIAMOND,       CARD_8_HEART,
		CARD_9_CLUB,       CARD_9_SPADE,       CARD_9_DIAMOND,       CARD_9_HEART,
		CARD_10_CLUB,      CARD_10_SPADE,      CARD_10_DIAMOND,      CARD_10_HEART,
		CARD_JACK_CLUB,    CARD_JACK_SPADE,    CARD_JACK_DIAMOND,    CARD_JACK_HEART,
		CARD_QUEEN_CLUB,   CARD_QUEEN_SPADE,   CARD_QUEEN_DIAMOND,   CARD_QUEEN_HEART,
		CARD_KING_CLUB,    CARD_KING_SPADE,    CARD_KING_DIAMOND,    CARD_KING_HEART,
		CARD_ACE_CLUB,     CARD_ACE_SPADE,     CARD_ACE_DIAMOND,     CARD_ACE_HEART,
	};
	
	public static String getColorDesc(CardColor color)
	{
		switch (color) {
			case CARD_COLOR_BLACK:
				return "Black";
			case CARD_COLOR_RED:
				return "Red";
			case CARD_COLOR_CLUB:
				return "Club";
			case CARD_COLOR_SPADE:
				return "Spade";
			case CARD_COLOR_DIAMOND:
				return "Diamond";
			case CARD_COLOR_HEART:
				return "Heart";
			default:
				return "<error>";
		}
	}
	
	public static String getFigureDesc(CardFigure figure)
	{
		switch (figure) {
		case CARD_FIGURE_JOKER:
			return "Joker";
		case CARD_FIGURE_2:
			return "2";
		case CARD_FIGURE_3:
			return "3";
		case CARD_FIGURE_4:
			return "4";
		case CARD_FIGURE_5:
			return "5";
		case CARD_FIGURE_6:
			return "6";
		case CARD_FIGURE_7:
			return "7";
		case CARD_FIGURE_8:
			return "8";
		case CARD_FIGURE_9:
			return "9";
		case CARD_FIGURE_10:
			return "10";
		case CARD_FIGURE_JACK:
			return "Jack";
		case CARD_FIGURE_QUEEN:
			return "Queen";
		case CARD_FIGURE_KING:
			return "King";
		case CARD_FIGURE_ACE:
			return "Ace";
		default:
			return "<error>";
		}
	}
	
	public static CardColor getColorEnum(String color)
	{
		if (color.equals("Club")) {
			return CardColor.CARD_COLOR_CLUB;
		}
		
		if (color.equals("Spade")) {
			return CardColor.CARD_COLOR_SPADE;
		}
		
		if (color.equals("Diamond")) {
			return CardColor.CARD_COLOR_DIAMOND;
		}
		
		if (color.equals("Heart")) {
			return CardColor.CARD_COLOR_HEART;
		}
		
		if (color.equals("Black")) {
			return CardColor.CARD_COLOR_BLACK;
		}
		
		if (color.equals("Red")) {
			return CardColor.CARD_COLOR_RED;
		}
		
		return CardColor.CARD_COLOR_NONE;
	}
	
	public static CardFigure getFigureEnum(String figure)
	{
		if (figure.equals("Joker")) {
			return CardFigure.CARD_FIGURE_JOKER;
		}
		
		if (figure.equals("2")) {
			return CardFigure.CARD_FIGURE_2;
		}
		
		if (figure.equals("3")) {
			return CardFigure.CARD_FIGURE_3;
		}
		
		if (figure.equals("4")) {
			return CardFigure.CARD_FIGURE_4;
		}
		
		if (figure.equals("5")) {
			return CardFigure.CARD_FIGURE_5;
		}
		
		if (figure.equals("6")) {
			return CardFigure.CARD_FIGURE_6;
		}
		
		if (figure.equals("7")) {
			return CardFigure.CARD_FIGURE_7;
		}
		
		if (figure.equals("8")) {
			return CardFigure.CARD_FIGURE_8;
		}
		
		if (figure.equals("9")) {
			return CardFigure.CARD_FIGURE_9;
		}
		
		if (figure.equals("10")) {
			return CardFigure.CARD_FIGURE_10;
		}
		
		if (figure.equals("Jack")) {
			return CardFigure.CARD_FIGURE_JACK;
		}
		
		if (figure.equals("Queen")) {
			return CardFigure.CARD_FIGURE_QUEEN;
		}
		
		if (figure.equals("King")) {
			return CardFigure.CARD_FIGURE_KING;
		}
		
		if (figure.equals("Ace")) {
			return CardFigure.CARD_FIGURE_ACE;
		}
		
		return CardFigure.CARD_FIGURE_NONE;
	}
}
