package makao.model.card;

import java.util.ArrayList;

public class CardCombo
{
	private ArrayList<Card> cardList;
	
	public CardCombo(ArrayList<Card> _cardList)
	{
		cardList = _cardList;
	}
	
	public ArrayList<Card> toList()
	{
		return cardList;
	}
	
	public int length()
	{
		return cardList.size();
	}
	
	public Card first()
	{
		if (cardList.size() > 0) {
			return cardList.get(0);
		}
		
		return null;
	}
	
	public Card last()
	{
		if (cardList.size() > 0) {
			return cardList.get(cardList.size() - 1);
		}
		
		return null;
	}
	
	// combo zawiera karty waleczne
	public boolean containsBattleCards()
	{
		for (int i = 0; i < cardList.size(); i++) {
			if (cardList.get(i).isFightCard()) {
				return true;
			}
		}
		
		return false;
	}
	
	// combo sklada sie z kart majacych taka sama figure
	public boolean allCardsHaveSameFigure()
	{
		for (int i = 0; i < cardList.size(); i++) {
			if (i > 0) {
				if (cardList.get(i).getFigure() != cardList.get(i - 1).getFigure()) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	// combo zawiera jokery
	public boolean containsJokers()
	{
		for (int i = 0; i < cardList.size(); i++) {
			if (cardList.get(i).isJoker()) {
				return true;
			}
		}
		
		return false;
	}
	
	// czy jokery w combo sa uzupelnione
	public boolean isJokersPreferenceFilled()
	{
		if (!containsJokers()) {
			return false;
		}
		
		boolean jokersPreferenceOk = true;
		for (int i = 0; i < cardList.size(); i++) {
			if (cardList.get(i).isJoker() && (cardList.get(i).getJokerPreference() == null)) {
				jokersPreferenceOk = false;
				break;
			}
		}
		
		return jokersPreferenceOk;
	}
	
	// podmienia jokery na ich preferencje
	public void replaceJokersWithPreference()
	{
		if (!containsJokers() || !isJokersPreferenceFilled()) {
			return;
		}
		
		for (int i = 0; i < cardList.size(); i++) {
			if (cardList.get(i).isJoker()) {
				if (cardList.get(i).getJokerPreference() != null) {
					if (!cardList.get(i).isExchanged()) {
						cardList.get(i).exchange();
					}
				}
			}
		}
	}
	
	// czy wszystkie karty sa waleczne
	public boolean allCardsAreBattle()
	{
		for (int i = 0; i < cardList.size(); i++) {
			if (!cardList.get(i).isFightCard()) {
				return false;
			}
		}
		
		return true;
	}
	
	// czy wszystkie karty sa waleczne i jest miedzy nimi przejscie
	public boolean allCardsAreBattleWithPass()
	{
		if (!allCardsAreBattle()) {
			return false;
		}
		
		// jest jakiejs przejscie miedzy kartami, kolor lub figura
		for (int i = 0; i < cardList.size(); i++) {
			if (i > 0) {
				if (CardComparator.compareCards(cardList.get(i), cardList.get(i - 1)) == CardCompareResult.COMPARE_RESULT_NONE) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	// laczna suma punktow kart walecznych w combo
	public int battleCardsStake()
	{
		int fightScore = 0;
		
		for (int i = 0; i < cardList.size(); i++) {
			if (!cardList.get(i).isFightCard()) {
				return -1;
			}
			
			fightScore += cardList.get(i).getCardPoints();
		}
		
		return fightScore;
	}
	
	// ilosc kolejek w combo
	public int numQueues()
	{
		if (!allCardsHaveSameFigure()) {
			return -1;
		} else {
			return cardList.size();
		}
	}
	
	// sprawdza czy na poczatku sa krole n.w. a potem krole w.
	public boolean isBattleCardsOrderValid()
	{
		for (int i = 0; i < cardList.size(); i++) {
			if (i > 0) {
				if (cardList.get(i - 1).isFightCard() && !cardList.get(i).isFightCard()) {
					// krol w.,  krol n.w. to jest zle
					return false;
				}
			}
		}
		return true;
	}
}
