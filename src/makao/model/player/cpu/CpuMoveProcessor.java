package makao.model.player.cpu;

import java.util.ArrayList;

import makao.logic.GameState;
import makao.logic.GameStateType;
import makao.model.card.Card;
import makao.model.card.CardColor;
import makao.model.card.CardCombo;
import makao.model.card.CardComparator;
import makao.model.card.CardCompareResult;
import makao.model.card.CardFigure;
import makao.model.player.Player;

public class CpuMoveProcessor
{
	private Player cpuPlayer;
	private GameState gameState;
	
	public CpuMoveProcessor(Player _cpuPlayer, GameState _gameState)
	{
		cpuPlayer = _cpuPlayer;
		gameState = _gameState;
	}
	
	private void updateJokers()
	{
		ArrayList<Card> cpuCards = cpuPlayer.getAllCards();
		for (int i = 0; i < cpuCards.size(); i++) {
			if (cpuCards.get(i).isJoker()) {
				
				// sprawdz czy nie ma jakis kart niejokerowych
				Card jokerPref = null;
				for (int j = 0; j < cpuCards.size(); j++) {
					if (!cpuCards.get(j).isJoker()) {
						jokerPref = cpuCards.get(j);
						break;
					}
				}
				
				if (jokerPref == null) {
					// no to chyba musi byc jeden joker, wiec zmien
					// go na to co jest na stole
					jokerPref = gameState.getBoardCard();
				}
				
				// teraz mozna zdecydowac jokera
				cpuCards.get(i).setJokerPreference(Card.getCardFromParams(jokerPref.getColor(), jokerPref.getFigure()));
			}
		}
	}
	
	public CardCombo getCardCombo()
	{
		updateJokers();
		GameStateType gameStateType = gameState.getGameState();
		if (gameStateType == GameStateType.GAME_STATE_TYPE_NORMAL)
		{
			return normalResponse();
		}
		else
		{
			switch (gameStateType)
			{
			case GAME_STATE_TYPE_BATTLE_STAKE:
				return battleStakeResponse();
				
			case GAME_STATE_TYPE_JACK_REQUEST:
				return jackRequestResponse();
				
			case GAME_STATE_TYPE_QUEUE_REQUEST:
				return queueResponse();
				
			default: case GAME_STATE_TYPE_NORMAL:
				return normalResponse();
			}
		}
	}
	
	private CardCombo normalResponse()
	{
		ArrayList<Card> cpuCards = cpuPlayer.getAllCards();
		ArrayList<Card> cardCombo = new ArrayList<Card>();
		for (int i = 0; i < cpuCards.size(); i++) {
			Card card = cpuCards.get(i);
			if (CardComparator.compareCards(card, gameState.getBoardCard()) != CardCompareResult.COMPARE_RESULT_NONE) {
				cardCombo.add(cpuCards.remove(i)); // wybierz karte komputera
				
				// narazie tylko po jednej karcie
				return new CardCombo(cardCombo);
			}
		}
		
		return new CardCombo(new ArrayList<Card>());
	}
	
	private CardCombo battleStakeResponse()
	{
		ArrayList<Card> cpuCards = cpuPlayer.getAllCards();
		ArrayList<Card> cardCombo = new ArrayList<Card>();
		
		for (int i = 0; i < cpuCards.size(); i++) {
			Card card = cpuCards.get(i);
			if (card.isFightCard()) {
				if (CardComparator.compareCards(card, gameState.getBoardCard()) != CardCompareResult.COMPARE_RESULT_NONE) {
					cardCombo.add(cpuCards.remove(i));
					
					// narazie tylko po jednej karcie
					return new CardCombo(cardCombo);
				}
			}
		}
		
		return new CardCombo(new ArrayList<Card>());
	}
	
	private CardCombo jackRequestResponse()
	{
		ArrayList<Card> cardCombo = new ArrayList<Card>();
		ArrayList<Card> cpuCards = cpuPlayer.getAllCards();
		CardFigure boardCardFigure = gameState.getBoardCard().getFigure();
		// ktos zada jopka
		
		if (boardCardFigure != CardFigure.CARD_FIGURE_JACK) {
			// nie ma juz jopka na stole ktos juz spelnil zadanie
			// szukaj zadanej karty
			
			for (int i = 0; i < cpuCards.size(); i++) {
				if ((cpuCards.get(i).getFigure() == boardCardFigure) && !cpuCards.get(i).isSpecial()) {
					cardCombo.add(cpuCards.remove(i)); // dodaj wszystkie pasujace karty
				}
			}
		} else {
			// jopek ciagle jest mozna podbic na to co sie ma
			// najpierw sprawdz czy sa jakies zwykle karty
			Card normalCard = null;
			
			for (int i = 0; i < cpuCards.size(); i++) {
				if (!cpuCards.get(i).isSpecial()) {
					normalCard = cpuCards.get(i);
					break;
				}
			}
			
			if (normalCard == null) {
				// jak nic nie ma no to niestety nie mamy jak przebic zadania
				// sprawdz czy jest jopek, mozna rzucic pod kolor tylko
				for (int i = 0; i < cpuCards.size(); i++) {
					if (cpuCards.get(i).isJack()) {
						cardCombo.add(cpuCards.remove(i)); // wal wszystkie :D HAHAHAHA
					}
				}
			} else {
				// tutaj mozna zadac karty ktora mamy, jesli mamy jeszcze jopka
				for (int i = 0; i < cpuCards.size(); i++) {
					if (cpuCards.get(i).isJack()) {
						cardCombo.add(cpuCards.remove(i)); // wal wszystkie :D HAHAHAHA
					}
				}
			}
		}
		
		return new CardCombo(cardCombo);
	}
	
	private CardCombo queueResponse()
	{
		ArrayList<Card> cardCombo = new ArrayList<Card>();
		// sprawdz czy sa kolejki
		ArrayList<Card> cpuCards = cpuPlayer.getAllCards();
		
		for (int i = 0; i < cpuCards.size(); i++) {
			if (cpuCards.get(i).isPause()) {
				cardCombo.add(cpuCards.remove(i));
				break;
			}
		}
		
		return new CardCombo(cardCombo);
	}
	
	public CardColor askForAceColor()
	{
		CardColor cardColor = null;
		ArrayList<Card> cpuCards = cpuPlayer.getAllCards();
		for (int i = 0; i < cpuCards.size(); i++) {
			if (!cpuCards.get(i).isJoker()) {
				cardColor = cpuCards.get(i).getColor();
				break;
			}
		}
		
		return cardColor;
	}
	
	public CardFigure askForJackFigure()
	{
		CardFigure cardFigure = null;
		ArrayList<Card> cpuCards = cpuPlayer.getAllCards();
		for (int i = 0; i < cpuCards.size(); i++) {
			if (!cpuCards.get(i).isSpecial()) {
				cardFigure = cpuCards.get(i).getFigure();
				break;
			}
		}
		
		return cardFigure;
	}
}
