package makao.model.player;

import java.util.ArrayList;

import makao.model.card.Card;
import makao.model.player.cpu.CpuPlayerLevel;

public class Player
{
	private PlayerType playerType;
	private String playerName;
	private CpuPlayerLevel cpuLevel;
	private int numQueues;
	
	private ArrayList<Card> cards = new ArrayList<Card>();
	
	private boolean makaoRaised;
	private boolean afterMakaoRaised;
	private boolean winnerStatus;
	
	public Player(String _playerName, PlayerType _playerType)
	{
		playerType = _playerType;
		playerName = _playerName;
		cpuLevel = CpuPlayerLevel.CPU_PLAYER_NOT_A_CPU;
		numQueues = 0;
		makaoRaised = false;
		afterMakaoRaised = false;
		winnerStatus = false;
	}
	
	public Player(String _playerName, PlayerType _playerType, CpuPlayerLevel _cpuLevel)
	{
		playerType = _playerType;
		playerName = _playerName;
		cpuLevel = _cpuLevel;
		numQueues = 0;
		makaoRaised = false;
		afterMakaoRaised = false;
		winnerStatus = false;
	}
	
	// wygrany
	
	public void setAsWinner()
	{
		winnerStatus = true;
	}
	
	public void clearWinnerStatus()
	{
		winnerStatus = false;
	}
	
	public boolean isWinner()
	{
		return winnerStatus;
	}
	
	// makao
	
	public void raiseMakao()
	{
		makaoRaised = true;
	}
	
	public void clearMakao()
	{
		makaoRaised = false;
	}
	
	public boolean wasMakaoRaised()
	{
		return makaoRaised;
	}
	
	// po makale
	
	public void raiseAfterMakao()
	{
		afterMakaoRaised = true;
	}
	
	public void clearAfterMakao()
	{
		afterMakaoRaised = false;
	}
	
	public boolean wasAfterMakaoRaised()
	{
		return afterMakaoRaised;
	}
	
	// interfejs do kart
	
	public void addCard(Card card)
	{
		cards.add(card);
	}
	
	public void removeCardAtIndex(int index)
	{
		cards.remove(index);
	}
	
	public Card peekCardAtIndex(int index)
	{
		return cards.get(index);
	}
	
	public Card selectCardAtIndex(int index)
	{
		return cards.remove(index);
	}
	
	public ArrayList<Card> getAllCards()
	{
		return cards;
	}
	
	// kolejki
	
	public void addQueue(int _numQueues)
	{
		numQueues = _numQueues;
	}
	
	public boolean isWaiting()
	{
		return numQueues > 0;
	}
	
	public int getQueues()
	{
		return numQueues;
	}
	
	// powinno zostac zmniejszone tuz po wywolaniu funkcji isWaiting()
	public void oneQueuePass()
	{
		if (numQueues > 0) {
			numQueues--;
		}
	}
	
	public boolean isPlaying()
	{
		return !isWaiting() && !isWinner();
	}
	
	public CpuPlayerLevel getCpuLevel()
	{
		return cpuLevel;
	}
	
	public PlayerType getPlayerType()
	{
		return playerType;
	}
	
	public String getPlayerName()
	{
		return playerName;
	}
	
	@Override
	public String toString()
	{
		// jfSeth: U/LV-0/##, 0 queues, 10 cards
		return playerName
			+ ": "
			+ Player.getPlayerTypeName(playerType).charAt(0)
			+ "/LV-"
			+ ((playerType == PlayerType.PLAYER_TYPE_USER) ? "0" : Player.getPlayerLevelNameShort(cpuLevel))
			+ "/"
			+ (makaoRaised ? "@" : "#")
			+ (afterMakaoRaised ? "@" : "#")
			+ ", "
			+ numQueues
			+ " queues, "
			+ cards.size()
			+ " cards"
			+ (winnerStatus ? ", winner" : "");
	}
	
	public static Player createCpuPlayer(String _playerName, CpuPlayerLevel _cpuLevel)
	{
		return new Player(_playerName, PlayerType.PLAYER_TYPE_CPU, _cpuLevel);
	}
	
	public static Player createUserPlayer(String _playerName)
	{
		return new Player(_playerName, PlayerType.PLAYER_TYPE_USER);
	}
	
	public static PlayerType getPlayerTypeEnum(String _playerType)
	{
		if (_playerType.equals("User"))
		{
			return PlayerType.PLAYER_TYPE_USER;
		}
		else if (_playerType.equals("Cpu"))
		{
			return PlayerType.PLAYER_TYPE_CPU;
		}
		else
		{
			return null;
		}
	}
	
	public static CpuPlayerLevel getCpuPlayerLevelEnum(String _cpuPlayerLevel)
	{
		if (_cpuPlayerLevel.equals("VeryEasy"))
		{
			return CpuPlayerLevel.CPU_PLAYER_LEVEL_VERY_EASY;
		}
		else if (_cpuPlayerLevel.equals("Easy"))
		{
			return CpuPlayerLevel.CPU_PLAYER_LEVEL_EASY;
		}
		else if (_cpuPlayerLevel.equals("Medium"))
		{
			return CpuPlayerLevel.CPU_PLAYER_LEVEL_MEDIUM;
		}
		else if (_cpuPlayerLevel.equals("Hard"))
		{
			return CpuPlayerLevel.CPU_PLAYER_LEVEL_HARD;
		}
		else if (_cpuPlayerLevel.equals("VeryHard"))
		{
			return CpuPlayerLevel.CPU_PLAYER_LEVEL_VERY_HARD;
		}
		else
		{
			return null;
		}
	}
	
	public static String getPlayerTypeName(PlayerType _playerType)
	{
		switch (_playerType) {
		case PLAYER_TYPE_CPU:
			return "Cpu";
			
		case PLAYER_TYPE_USER:
			return "User";
			
		default: return "";
		}
	}
	
	public static String getPlayerLevelNameShort(CpuPlayerLevel _cpuPlayerLevel)
	{
		switch (_cpuPlayerLevel)
		{
		case CPU_PLAYER_LEVEL_VERY_EASY:
			return "VE";
		
		case CPU_PLAYER_LEVEL_EASY:
			return "E";
			
		case CPU_PLAYER_LEVEL_MEDIUM:
			return "M";
			
		case CPU_PLAYER_LEVEL_HARD:
			return "H";
			
		case CPU_PLAYER_LEVEL_VERY_HARD:
			return "VH";
			
		case CPU_PLAYER_NOT_A_CPU:
			return "N/A";
			
		default: return "";
		}
	}
	
	public static String getPlayerLevelName(CpuPlayerLevel _cpuPlayerLevel)
	{
		switch (_cpuPlayerLevel)
		{
		case CPU_PLAYER_LEVEL_VERY_EASY:
			return "VeryEasy";
		
		case CPU_PLAYER_LEVEL_EASY:
			return "Easy";
			
		case CPU_PLAYER_LEVEL_MEDIUM:
			return "Medium";
			
		case CPU_PLAYER_LEVEL_HARD:
			return "Hard";
			
		case CPU_PLAYER_LEVEL_VERY_HARD:
			return "VeryHard";
			
		case CPU_PLAYER_NOT_A_CPU:
			return "N/A";
			
		default: return "";
		}
	}
}
