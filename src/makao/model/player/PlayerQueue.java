package makao.model.player;

import java.util.ArrayList;

public class PlayerQueue
{
	private ArrayList<Player> players = new ArrayList<Player>();
	private int currentPlayerPointer = 0;
	
	public PlayerQueue()
	{
		
	}
	
	public ArrayList<Player> getCurrentlyPlayingPlayers()
	{
		ArrayList<Player> playingPlayers = new ArrayList<Player>();
		for (int i = 0; i < players.size(); i++) {
			if (players.get(i).isPlaying()) {
				playingPlayers.add(players.get(i));
			}
		}
		
		return playingPlayers;
	}
	
	public boolean weHaveLooser()
	{
		int numWinners = 0;
		for (int i = 0; i < players.size(); i++) {
			if (players.get(i).isWinner()) {
				numWinners++;
			}
		}
		
		return (numWinners == (players.size() - 1));
	}
	
	public ArrayList<Player> getAllPlayers()
	{
		return players;
	}
	
	public void clearQueue()
	{
		players.clear();
		currentPlayerPointer = 0;
	}
	
	public void addPlayer(Player _player)
	{
		players.add(_player);
	}
	
	public void nextPlayer()
	{
		int numOfPlayers = players.size();
		
		currentPlayerPointer = (currentPlayerPointer + 1) % numOfPlayers;
	}
	
	public void previousPlayer()
	{
		int numOfPlayers = players.size();
		
		currentPlayerPointer = (currentPlayerPointer + (numOfPlayers - 1)) % numOfPlayers;
	}
	
	public Player getCurrentPlayer()
	{
		return players.get(currentPlayerPointer);
	}
	
	public int getCurrentPlayerPointer()
	{
		return currentPlayerPointer;
	}
}
