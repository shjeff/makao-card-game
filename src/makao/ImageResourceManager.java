package makao;

import java.io.InputStream;
import java.util.Hashtable;

import javax.swing.JOptionPane;

public class ImageResourceManager
{
	public String resourcesBasePath = "";
	
	public boolean isRunningInsideJar = false;
	
	public Class mainClassLoader;
	
	private static ImageResourceManager instance = null;
	
	public static String getResourceFilePath(int _resourceId)
	{
		return resourceList.get(_resourceId);
	}
	
	public static void Initialize(Class _mainClassLoader, String _resourcesBasePath, boolean _isRunningInsideJar)
	{
		if (instance == null) {
			instance = new ImageResourceManager(_mainClassLoader, _resourcesBasePath, _isRunningInsideJar);
		}
	}
	
	public static ImageResourceManager getInstance()
	{
		return instance;
	}
	
	private ImageResourceManager(Class _mainClassLoader, String _resourcesBasePath, boolean _isRunningInsideJar)
	{
		mainClassLoader = _mainClassLoader;
		resourcesBasePath = _resourcesBasePath;
		isRunningInsideJar = _isRunningInsideJar;
	}
	
	public boolean isRunningFromJar()
	{
		return isRunningInsideJar;
	}
	
	public String getResourcesEnvBasePath()
	{
		return resourcesBasePath;
	}
	
	public InputStream getResourceInputStream(String _filePath)
	{
		// base points to /res 
		// main is in makao.MakaoCardGame
		_filePath = _filePath.replace('\\', '/');
		return mainClassLoader.getResourceAsStream("/res/" + _filePath);
	}
	
	public static final int IMG_RES_ID_GS_QUEUE_INACTIVE = 0;
	
	public static final int IMG_RES_ID_GS_QUEUE_ACTIVE = 1;
	
	public static final int IMG_RES_ID_GS_BATTLE_INACTIVE = 2;
	
	public static final int IMG_RES_ID_GS_BATTLE_ACTIVE = 3;
	
	public static final int IMG_RES_ID_GS_COLOR_CLUB = 4;
	
	public static final int IMG_RES_ID_GS_COLOR_SPADE = 5;
	
	public static final int IMG_RES_ID_GS_COLOR_DIAMOND = 6;
	
	public static final int IMG_RES_ID_GS_COLOR_HEART = 7;
	
	public static final int IMG_RES_ID_GS_REQ_NONE = 8;
	
	public static final int IMG_RES_ID_GS_REQ_5 = 9;
	
	public static final int IMG_RES_ID_GS_REQ_6 = 10;
	
	public static final int IMG_RES_ID_GS_REQ_7 = 11;
	
	public static final int IMG_RES_ID_GS_REQ_8 = 12;
	
	public static final int IMG_RES_ID_GS_REQ_9 = 13;
	
	public static final int IMG_RES_ID_GS_REQ_10 = 14;
	
	public static final int IMG_RES_ID_GS_REQ_QUEEN = 15;
	
	public static final int IMG_RES_ID_GS_REQ_KING = 16;
	
	@SuppressWarnings("serial")
	private static Hashtable<Integer, String> resourceList = new Hashtable<Integer, String>() {{
		put(IMG_RES_ID_GS_QUEUE_INACTIVE, "gamestate\\queue\\queue-inactive.png");
		put(IMG_RES_ID_GS_QUEUE_ACTIVE, "gamestate\\queue\\queue-active.png");
		put(IMG_RES_ID_GS_BATTLE_INACTIVE, "gamestate\\battle\\battle-inactive.png");
		put(IMG_RES_ID_GS_BATTLE_ACTIVE, "gamestate\\battle\\battle-active.png");
		put(IMG_RES_ID_GS_COLOR_CLUB, "gamestate\\color\\color-club.png");
		put(IMG_RES_ID_GS_COLOR_SPADE, "gamestate\\color\\color-spade.png");
		put(IMG_RES_ID_GS_COLOR_DIAMOND, "gamestate\\color\\color-diamond.png");
		put(IMG_RES_ID_GS_COLOR_HEART, "gamestate\\color\\color-heart.png");
		put(IMG_RES_ID_GS_REQ_NONE, "gamestate\\requests\\req-none.png");
		put(IMG_RES_ID_GS_REQ_5, "gamestate\\requests\\req-5.png");
		put(IMG_RES_ID_GS_REQ_6, "gamestate\\requests\\req-6.png");
		put(IMG_RES_ID_GS_REQ_7, "gamestate\\requests\\req-7.png");
		put(IMG_RES_ID_GS_REQ_8, "gamestate\\requests\\req-8.png");
		put(IMG_RES_ID_GS_REQ_9, "gamestate\\requests\\req-9.png");
		put(IMG_RES_ID_GS_REQ_10, "gamestate\\requests\\req-10.png");
		put(IMG_RES_ID_GS_REQ_QUEEN, "gamestate\\requests\\req-queen.png");
		put(IMG_RES_ID_GS_REQ_KING, "gamestate\\requests\\req-king.png");
	}};
}
