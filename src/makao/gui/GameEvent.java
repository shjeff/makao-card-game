package makao.gui;

public class GameEvent
{
	private int gameEvent;
	private String gameEventMessage;
	
	private GameEvent(int _gameEvent, String _gameEventMessage)
	{
		gameEvent = _gameEvent;
		gameEventMessage = _gameEventMessage;
	}
	
	public int getEventID()
	{
		return gameEvent;
	}
	
	public String getEventMessage()
	{
		return gameEventMessage;
	}
	
	public static final String GAME_EVENT_SPLIT = "";

	// Game has been started
	public static final int GAME_EVENT_START_GAME = 0;
	
	// Round for player: abc
	public static final int GAME_EVENT_PLAYER_ROUND = 1;
	
	// Player '%CP' puts card %1  
	public static final int GAME_EVENT_PLAYER_PUTS_CARD = 2;
	
	// Current points of battle cards: 5
	public static final int GAME_EVENT_BATTLE_STATE_POINTS = 3;
	
	// Player '%CP' says MAKAO
	public static final int GAME_EVENT_PLAYER_SAYS_MAKAO = 4;
	
	// Player '%CP' says AFTER MAKAO
	public static final int GAME_EVENT_PLAYER_SAYS_AFTER_MAKAO = 5;
	
	// CHECK: MAKAO was told
	public static final int GAME_EVENT_CHECK_MAKAO_WAS_TOLD = 6;
	
	// CHECK: MAKAO was not told
	public static final int GAME_EVENT_CHECK_MAKAO_WAS_NOT_TOLD = 7;
	
	// CHECK: AFTER MAKAO was told
	public static final int GAME_EVENT_CHECK_AFTER_MAKAO_WAS_TOLD = 8;
	
	// CHECK: AFTER MAKAO was not told
	public static final int GAME_EVENT_CHECK_AFTER_MAKAO_WAS_NOT_TOLD = 9;
	
	// CLEARED: MAKAO
	public static final int GAME_EVENT_CLEARED_MAKAO = 10;
	
	// CLEARED: AFTER MAKAO
	public static final int GAME_EVENT_CLEARED_AFTER_MAKAO = 11;
	
	// Player '%CP' got punish (5 cards) for not saying MAKAO
	public static final int GAME_EVENT_PLAYER_GOT_PUNISH_MAKAO_WAS_NOT_TOLD = 12; 
	
	// Player '%CP' got punish (5 cards) for not saying AFTER MAKAO
	public static final int GAME_EVENT_PLAYER_GOT_PUNISH_AFTER_MAKAO_WAS_NOT_TOLD = 13;
	
	// Player '%CP' wins
	public static final int GAME_EVENT_PLAYER_WINS = 14;
	
	// Player '%CP' is looser
	public static final int GAME_EVENT_GAME_LOOSER = 15;
	
	// Player '%CP' gets %1 cards
	public static final int GAME_EVENT_PLAYER_GETS_CARDS = 16;
	
	// Battle cards for previous player
	public static final int GAME_EVENT_BATTLE_FOR_PREVIOUS_PLAYER = 17;
	
	// Game has been finished
	public static final int GAME_EVENT_FINISHED = 18;
	
	// End of player round: abc
	public static final int GAME_EVENT_END_PLAYER_ROUND = 19;
	
	// Switching to next player
	public static final int GAME_EVENT_PLAYER_CHANGE = 20;
	
	// Player is waiting (queue), left: %1 queues
	public static final int GAME_EVENT_PLAYER_WAITING_QUEUE = 21;
	
	// Player is waiting (winner)
	public static final int GAME_EVENT_PLAYER_WAITING_WINNER = 22;
	
	// Last card on board: %CRD
	public static final int GAME_EVENT_LAST_CARD_ON_THE_BOARD = 23;
	
	// Changed game color to %1
	public static final int GAME_EVENT_CHANGED_GAME_COLOR = 24;
	
	// Player '%CP' requests %1
	public static final int GAME_EVENT_PLAYER_JACK_REQUEST = 25;
	
	// Player '%CP' got punish (5 cards) for bad request
	public static final int GAME_EVENT_PLAYER_GOT_PUNISH_BAD_JACK_REQUEST = 26;
	
	// Player '%CP' got %1 wait queues
	public static final int GAME_EVENT_PLAYER_GOT_PUNISH_WAIT_QUEUES = 27;
	
	// Player '%CP' takes 1 card
	public static final int GAME_EVENT_PLAYER_TAKES_ONE_CARD = 28;
	
	// Current Jack figure request: %11
	public static final int GAME_EVENT_CURRENT_REQUEST = 29; 
	
	// Current queues: %1
	public static final int GAME_EVENT_CURRENT_QUEUES = 30;
	
	// Issued new card stack
	public static final int GAME_EVENT_ISSUED_NEW_CARD_STACK = 31;

	

	// Game has been started
	private static GameEvent GAME_EVENT_MSG_START_GAME = new GameEvent(GAME_EVENT_START_GAME, "Game has been started");
	
	// Round for player: abc
	private static GameEvent GAME_EVENT_MSG_PLAYER_ROUND = new GameEvent(GAME_EVENT_PLAYER_ROUND, "Round for player: %CP");
	
	// Player '%CP' puts card %1
	private static GameEvent GAME_EVENT_MSG_PLAYER_PUTS_CARD = new GameEvent(GAME_EVENT_PLAYER_PUTS_CARD, "Player '%CP' puts card %1");
	
	// Points of battle cards: %GBP
	private static GameEvent GAME_EVENT_MSG_BATTLE_STATE_POINTS = new GameEvent(GAME_EVENT_BATTLE_STATE_POINTS, "Points of battle cards: %GBP");
	
	// Player '%CP' says MAKAO
	private static GameEvent GAME_EVENT_MSG_PLAYER_SAYS_MAKAO = new GameEvent(GAME_EVENT_PLAYER_SAYS_MAKAO, "Player '%CP' says MAKAO");
	
	// Player '%CP' says AFTER MAKAO
	private static GameEvent GAME_EVENT_MSG_PLAYER_SAYS_AFTER_MAKAO = new GameEvent(GAME_EVENT_PLAYER_SAYS_AFTER_MAKAO, "Player '%CP' says AFTER MAKAO");
	
	// CHECK: MAKAO was told
	private static GameEvent GAME_EVENT_MSG_CHECK_MAKAO_WAS_TOLD = new GameEvent(GAME_EVENT_CHECK_MAKAO_WAS_TOLD, "CHECK: MAKAO was told");
	
	// CHECK: MAKAO was not told
	private static GameEvent GAME_EVENT_MSG_CHECK_MAKAO_WAS_NOT_TOLD = new GameEvent(GAME_EVENT_CHECK_MAKAO_WAS_NOT_TOLD, "CHECK: MAKAO was not told");
	
	// CHECK: AFTER MAKAO was told
	private static GameEvent GAME_EVENT_MSG_CHECK_AFTER_MAKAO_WAS_TOLD = new GameEvent(GAME_EVENT_CHECK_AFTER_MAKAO_WAS_TOLD, "CHECK: AFTER MAKAO was told");
	
	// CHECK: AFTER MAKAO was not told
	private static GameEvent GAME_EVENT_MSG_CHECK_AFTER_MAKAO_WAS_NOT_TOLD = new GameEvent(GAME_EVENT_CHECK_AFTER_MAKAO_WAS_NOT_TOLD, "CHECK: AFTER MAKAO was not told");
	
	// CLEARED: MAKAO
	private static GameEvent GAME_EVENT_MSG_CLEARED_MAKAO = new GameEvent(GAME_EVENT_CLEARED_MAKAO, "CLEARED: MAKAO");
	
	// CLEARED: AFTER MAKAO, chyba not used
	private static GameEvent GAME_EVENT_MSG_CLEARED_AFTER_MAKAO = new GameEvent(GAME_EVENT_CLEARED_AFTER_MAKAO, "CLEARED: AFTER MAKAO");
	
	// Player '%CP' got punish (5 cards) for not saying MAKAO
	private static GameEvent GAME_EVENT_MSG_PLAYER_GOT_PUNISH_MAKAO_WAS_NOT_TOLD = new GameEvent(GAME_EVENT_PLAYER_GOT_PUNISH_MAKAO_WAS_NOT_TOLD, "Player '%CP' got punish (5 cards) for not saying MAKAO");
	
	// Player '%CP' got punish (5 cards) for not saying AFTER MAKAO
	private static GameEvent GAME_EVENT_MSG_PLAYER_GOT_PUNISH_AFTER_MAKAO_WAS_NOT_TOLD = new GameEvent(GAME_EVENT_PLAYER_GOT_PUNISH_AFTER_MAKAO_WAS_NOT_TOLD, "Player '%CP' got punish (5 cards) for not saying AFTER MAKAO");
	
	// Player '%CP' wins
	private static GameEvent GAME_EVENT_MSG_PLAYER_WINS = new GameEvent(GAME_EVENT_PLAYER_WINS, "Player '%CP' wins");
	
	// Player '%CP' is looser
	private static GameEvent GAME_EVENT_MSG_GAME_LOOSER = new GameEvent(GAME_EVENT_GAME_LOOSER, "Player '%CP' is looser");
	
	// Player '%CP' gets %PBP cards
	private static GameEvent GAME_EVENT_MSG_PLAYER_GETS_CARDS = new GameEvent(GAME_EVENT_PLAYER_GETS_CARDS, "Player '%CP' gets %1 cards");
	
	// Battle cards for previous player
	private static GameEvent GAME_EVENT_MSG_BATTLE_FOR_PREVIOUS_PLAYER = new GameEvent(GAME_EVENT_BATTLE_FOR_PREVIOUS_PLAYER, "Battle cards for previous player");
	
	// Game has been finished
	private static GameEvent GAME_EVENT_MSG_FINISHED = new GameEvent(GAME_EVENT_FINISHED, "Game has been finished");
	
	// End of player round
	private static GameEvent GAME_EVENT_MSG_END_PLAYER_ROUND = new GameEvent(GAME_EVENT_END_PLAYER_ROUND, "End of player round '%CP'");
	
	// Switching to next player
	private static GameEvent GAME_EVENT_MSG_PLAYER_CHANGE = new GameEvent(GAME_EVENT_PLAYER_CHANGE, "Switching to next player");

	// Player '%1' is waiting (queue), left: %2 queues
	private static GameEvent GAME_EVENT_MSG_PLAYER_WAITING_QUEUE = new GameEvent(GAME_EVENT_PLAYER_WAITING_QUEUE, "Player '%1' is waiting (queue), left: %2 queues");
	
	// Player '%1' is not playing (winner)
	private static GameEvent GAME_EVENT_MSG_PLAYER_WAITING_WINNER = new GameEvent(GAME_EVENT_PLAYER_WAITING_WINNER, "Player '%1' is not playing (winner)");
	
	// Last card on board: %CRD
	private static GameEvent GAME_EVENT_MSG_LAST_CARD_ON_THE_BOARD = new GameEvent(GAME_EVENT_LAST_CARD_ON_THE_BOARD, "Last card on board: %CRD");
	
	// Changed game color to: %1
	private static GameEvent GAME_EVENT_MSG_CHANGED_GAME_COLOR = new GameEvent(GAME_EVENT_CHANGED_GAME_COLOR, "Changed game color to: %1");
	
	// Player '%CP' requests: %1
	private static GameEvent GAME_EVENT_MSG_PLAYER_JACK_REQUEST = new GameEvent(GAME_EVENT_PLAYER_JACK_REQUEST, "Player '%CP' requests: %1");
	
	// Player '%CP' got punish (5 cards) for bad request
	private static GameEvent GAME_EVENT_MSG_PLAYER_GOT_PUNISH_BAD_JACK_REQUEST = new GameEvent(GAME_EVENT_PLAYER_GOT_PUNISH_BAD_JACK_REQUEST, "Player '%CP' got punish (5 cards) for bad request");
	
	// Player '%CP' got %1 wait queues
	private static GameEvent GAME_EVENT_MSG_PLAYER_GOT_PUNISH_WAIT_QUEUES = new GameEvent(GAME_EVENT_PLAYER_GOT_PUNISH_WAIT_QUEUES, "Player '%CP' got %1 wait queues");
	
	// Player '%CP' takes 1 card
	private static GameEvent GAME_EVENT_MSG_PLAYER_TAKES_ONE_CARD = new GameEvent(GAME_EVENT_PLAYER_TAKES_ONE_CARD, "Player '%CP' takes 1 card");
	
	// Current Jack figure request: %1
	private static GameEvent GAME_EVENT_MSG_CURRENT_REQUEST = new GameEvent(GAME_EVENT_CURRENT_REQUEST, "Current Jack figure request: %1");
	
	// Current queues: %1
	private static GameEvent GAME_EVENT_MSG_CURRENT_QUEUES = new GameEvent(GAME_EVENT_CURRENT_QUEUES, "Current queues: %1");
	
	// Issued new card stack
	private static GameEvent GAME_EVENT_MSG_ISSUED_NEW_CARD_STACK = new GameEvent(GAME_EVENT_ISSUED_NEW_CARD_STACK, "Issued new card stack");
	
	public static String getMessage(int _gameEvent)
	{
		switch (_gameEvent)
		{
			case GAME_EVENT_START_GAME: return GAME_EVENT_MSG_START_GAME.getEventMessage();
			case GAME_EVENT_PLAYER_ROUND: return GAME_EVENT_MSG_PLAYER_ROUND.getEventMessage();
			case GAME_EVENT_PLAYER_PUTS_CARD: return GAME_EVENT_MSG_PLAYER_PUTS_CARD.getEventMessage();
			case GAME_EVENT_BATTLE_STATE_POINTS: return GAME_EVENT_MSG_BATTLE_STATE_POINTS.getEventMessage();
			case GAME_EVENT_PLAYER_SAYS_MAKAO: return GAME_EVENT_MSG_PLAYER_SAYS_MAKAO.getEventMessage();
			case GAME_EVENT_PLAYER_SAYS_AFTER_MAKAO: return GAME_EVENT_MSG_PLAYER_SAYS_AFTER_MAKAO.getEventMessage();
			case GAME_EVENT_CHECK_MAKAO_WAS_TOLD: return GAME_EVENT_MSG_CHECK_MAKAO_WAS_TOLD.getEventMessage();
			case GAME_EVENT_CHECK_MAKAO_WAS_NOT_TOLD: return GAME_EVENT_MSG_CHECK_MAKAO_WAS_NOT_TOLD.getEventMessage();
			case GAME_EVENT_CHECK_AFTER_MAKAO_WAS_TOLD: return GAME_EVENT_MSG_CHECK_AFTER_MAKAO_WAS_TOLD.getEventMessage();
			case GAME_EVENT_CHECK_AFTER_MAKAO_WAS_NOT_TOLD: return GAME_EVENT_MSG_CHECK_AFTER_MAKAO_WAS_NOT_TOLD.getEventMessage();
			case GAME_EVENT_CLEARED_MAKAO: return GAME_EVENT_MSG_CLEARED_MAKAO.getEventMessage();
			case GAME_EVENT_CLEARED_AFTER_MAKAO: return GAME_EVENT_MSG_CLEARED_AFTER_MAKAO.getEventMessage();
			case GAME_EVENT_PLAYER_GOT_PUNISH_MAKAO_WAS_NOT_TOLD: return GAME_EVENT_MSG_PLAYER_GOT_PUNISH_MAKAO_WAS_NOT_TOLD.getEventMessage();
			case GAME_EVENT_PLAYER_GOT_PUNISH_AFTER_MAKAO_WAS_NOT_TOLD: return GAME_EVENT_MSG_PLAYER_GOT_PUNISH_AFTER_MAKAO_WAS_NOT_TOLD.getEventMessage();
			case GAME_EVENT_PLAYER_WINS: return GAME_EVENT_MSG_PLAYER_WINS.getEventMessage();
			case GAME_EVENT_GAME_LOOSER: return GAME_EVENT_MSG_GAME_LOOSER.getEventMessage();
			case GAME_EVENT_PLAYER_GETS_CARDS: return GAME_EVENT_MSG_PLAYER_GETS_CARDS.getEventMessage();
			case GAME_EVENT_BATTLE_FOR_PREVIOUS_PLAYER: return GAME_EVENT_MSG_BATTLE_FOR_PREVIOUS_PLAYER.getEventMessage();
			case GAME_EVENT_FINISHED: return GAME_EVENT_MSG_FINISHED.getEventMessage();
			case GAME_EVENT_END_PLAYER_ROUND: return GAME_EVENT_MSG_END_PLAYER_ROUND.getEventMessage();
			case GAME_EVENT_PLAYER_CHANGE: return GAME_EVENT_MSG_PLAYER_CHANGE.getEventMessage();
			case GAME_EVENT_PLAYER_WAITING_QUEUE: return GAME_EVENT_MSG_PLAYER_WAITING_QUEUE.getEventMessage();
			case GAME_EVENT_PLAYER_WAITING_WINNER: return GAME_EVENT_MSG_PLAYER_WAITING_WINNER.getEventMessage();
			case GAME_EVENT_LAST_CARD_ON_THE_BOARD: return GAME_EVENT_MSG_LAST_CARD_ON_THE_BOARD.getEventMessage();
			case GAME_EVENT_CHANGED_GAME_COLOR: return GAME_EVENT_MSG_CHANGED_GAME_COLOR.getEventMessage();
			case GAME_EVENT_PLAYER_JACK_REQUEST: return GAME_EVENT_MSG_PLAYER_JACK_REQUEST.getEventMessage();
			case GAME_EVENT_PLAYER_GOT_PUNISH_BAD_JACK_REQUEST: return GAME_EVENT_MSG_PLAYER_GOT_PUNISH_BAD_JACK_REQUEST.getEventMessage();
			case GAME_EVENT_PLAYER_GOT_PUNISH_WAIT_QUEUES: return GAME_EVENT_MSG_PLAYER_GOT_PUNISH_WAIT_QUEUES.getEventMessage();
			case GAME_EVENT_PLAYER_TAKES_ONE_CARD: return GAME_EVENT_MSG_PLAYER_TAKES_ONE_CARD.getEventMessage();
			case GAME_EVENT_CURRENT_REQUEST: return GAME_EVENT_MSG_CURRENT_REQUEST.getEventMessage();
			case GAME_EVENT_CURRENT_QUEUES: return GAME_EVENT_MSG_CURRENT_QUEUES.getEventMessage();
			case GAME_EVENT_ISSUED_NEW_CARD_STACK: return GAME_EVENT_MSG_ISSUED_NEW_CARD_STACK.getEventMessage();
			default: return "<error>";
		}
	}
}
