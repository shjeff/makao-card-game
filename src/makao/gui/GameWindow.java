package makao.gui;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Graphics;

import javax.swing.JScrollPane;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import makao.model.card.Card;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.GridLayout;
import javax.swing.JSplitPane;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JProgressBar;
import javax.swing.ScrollPaneConstants;

@SuppressWarnings("serial")
public class GameWindow extends JFrame
{
	private JList<String> lst_playerList = new JList<String>(new DefaultListModel<String>());
	private JTextArea gameLog = new JTextArea();
	
	private JButton btn_addPlayer = new JButton("Add player");
	private JButton btn_startGame = new JButton("Start new game");
	private JButton btn_endMove = new JButton("End move");
	private JButton btn_undoMove = new JButton("Undo move");
	private JButton btn_sayMakao = new JButton("Say makao");
	private JButton btn_sayAfterMakao = new JButton("Say after makao");
	private JProgressBar cpuProgressBar = new JProgressBar();
	
	private JPanel userCardsPanel = new JPanel();
	
	private JPanel boardCardPanel = new JPanel();
	private JPanel pickupCardPanel = new JPanel();
	
	private JPanel userCardComboPanel = new JPanel();
	private JPanel cpuCardsComboPanel = new JPanel();
	private JPanel gameInfoPanel = new JPanel();
	
	private JPanel gameColorPanel = new JPanel();
	private JPanel gameRequestPanel = new JPanel();
	private JPanel gameBattlePointsPanel = new JPanel();
	private JPanel gameWaitQueuePanel = new JPanel();
	
	public GameWindow()
	{
		setTitle("Makao GUI test");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(897, 579);
		setLocation(100, 100);
		getContentPane().setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel upperGameTableWindow = new JPanel();
		JPanel lowerGameTableWindow = new JPanel();
		JPanel centerGameTableWindow = new JPanel();
		
		upperGameTableWindow.setBackground(Color.black);
		lowerGameTableWindow.setBackground(Color.black);
		centerGameTableWindow.setBackground(Color.black);
		cpuCardsComboPanel.setBackground(Color.black);
		userCardComboPanel.setBackground(Color.black);
		userCardsPanel.setBackground(Color.DARK_GRAY);
		boardCardPanel.setBackground(Color.black);
		pickupCardPanel.setBackground(Color.black);
		
		userCardsPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		gameInfoPanel.setBackground(Color.BLACK);
		
		centerGameTableWindow.add(boardCardPanel);
		centerGameTableWindow.add(pickupCardPanel);
		
		lowerGameTableWindow.add(userCardComboPanel);
		upperGameTableWindow.setLayout(new GridLayout(1, 1, 5, 5));
		//upperGameTableWindow.add(cpuCardsComboPanel);
		upperGameTableWindow.add(gameInfoPanel);
		gameInfoPanel.setLayout(new GridLayout(0, 4, 0, 0));
		gameColorPanel.setBackground(Color.BLACK);
		
		gameInfoPanel.add(gameColorPanel);
		gameRequestPanel.setBackground(Color.BLACK);
		
		gameInfoPanel.add(gameRequestPanel);
		gameBattlePointsPanel.setBackground(Color.BLACK);
		
		gameInfoPanel.add(gameBattlePointsPanel);
		gameWaitQueuePanel.setBackground(Color.BLACK);
		
		gameInfoPanel.add(gameWaitQueuePanel);
		
		JPanel gameTableWindow = new JPanel();
		gameTableWindow.setLayout(new GridLayout(3, 0));
		gameTableWindow.add(upperGameTableWindow);
		
		gameTableWindow.add(centerGameTableWindow);
		gameTableWindow.add(lowerGameTableWindow);
		
		JPanel userControlsPanel = new JPanel();
		userControlsPanel.setLayout(new GridLayout(1, 0, 0, 0));
		userControlsPanel.add(btn_addPlayer);
		userControlsPanel.add(btn_startGame);
		userControlsPanel.add(btn_endMove);
		userControlsPanel.add(btn_undoMove);
		userControlsPanel.add(btn_sayMakao);
		userControlsPanel.add(btn_sayAfterMakao);
		
		JScrollPane userCardsScrollPanel = new JScrollPane(userCardsPanel);
		
		JSplitPane userGameSplitPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT, gameTableWindow, userCardsScrollPanel);
		userGameSplitPanel.setResizeWeight(0.8);
		
		JPanel userGamePanel = new JPanel();
		userGamePanel.setLayout(new BoxLayout(userGamePanel, BoxLayout.X_AXIS));
		userGamePanel.add(userGameSplitPanel);
		
		JPanel windowRightPanel = new JPanel();
		windowRightPanel.setLayout(new BorderLayout(0, 0));
		windowRightPanel.add(userControlsPanel, BorderLayout.SOUTH);
		windowRightPanel.add(userGamePanel, BorderLayout.CENTER);
		
		JScrollPane playerListScrollPanel = new JScrollPane(lst_playerList);
		playerListScrollPanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		playerListScrollPanel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		JScrollPane gameLogScrollPanel = new JScrollPane(gameLog);
		gameLogScrollPanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		gameLogScrollPanel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		JPanel gameInfoPanel = new JPanel();
		gameInfoPanel.setLayout(new BorderLayout(0, 0));
		gameInfoPanel.add(gameLogScrollPanel, BorderLayout.CENTER);
		gameInfoPanel.add(cpuProgressBar, BorderLayout.SOUTH);
		
		JSplitPane windowLeftPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT, playerListScrollPanel, gameInfoPanel);
		windowLeftPanel.setResizeWeight(0.5);
		windowLeftPanel.setOrientation(JSplitPane.VERTICAL_SPLIT);
		
		/* Glowne okno */
		JSplitPane mainWindowSplitPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, windowLeftPanel, windowRightPanel);
		mainWindowSplitPanel.setResizeWeight(0.5);
		getContentPane().add(mainWindowSplitPanel);
	}
	
	public void setGUIComponents(UserInterfaceManager _uiManager)
	{
		btn_addPlayer.addActionListener(_uiManager);
		btn_startGame.addActionListener(_uiManager);
		btn_endMove.addActionListener(_uiManager);
		btn_undoMove.addActionListener(_uiManager);
		btn_sayMakao.addActionListener(_uiManager);
		btn_sayAfterMakao.addActionListener(_uiManager);
		
		_uiManager.setButtonUIComponents(btn_addPlayer, btn_startGame, btn_endMove, btn_undoMove, btn_sayMakao, btn_sayAfterMakao);
		_uiManager.setGameLogUIComponent(gameLog);
		_uiManager.setPanelUIComponents(userCardsPanel, boardCardPanel, pickupCardPanel, userCardComboPanel, cpuCardsComboPanel, gameColorPanel, gameRequestPanel, gameBattlePointsPanel, gameWaitQueuePanel);
		_uiManager.setPlayerListUIComponent(lst_playerList);
		_uiManager.setProgressBarUIComponent(cpuProgressBar);
	}
}
