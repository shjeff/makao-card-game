package makao.gui;

import java.awt.AlphaComposite;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import makao.ImageResourceManager;
import makao.model.card.Card;

@SuppressWarnings("serial")
public class GameCardImage extends GameImage
{
	private Card card;
	private int index;
	private GameCardImageType gameCardImageType;
	
	private float opacity;
	private boolean changeOpacity;
	
	// mozna dorobic jakis typ zamiast inside combo
	public GameCardImage(Card _card, boolean _clickable, GameCardImageType _gameCardImageType)
	{
		super(_card.getCardImageFileName());
		
		setSize(gameImage.getWidth(), gameImage.getHeight());
		setPreferredSize(new Dimension(gameImage.getWidth(), gameImage.getHeight()));
		
		if (_clickable) {
			setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		}
		
		if (_gameCardImageType == GameCardImageType.GAME_CARD_IMAGE_TYPE_BOARD_CARD_HISTORY) {
			changeOpacity = true;
			opacity = 0.5f;
		} else {
			changeOpacity = false;
		}
		
		card = _card;
		gameCardImageType = _gameCardImageType;
		index = -1;
	}
	
	public Card getCard()
	{
		return card;
	}
	
	public GameCardImageType getCardImageType()
	{
		return gameCardImageType;
	}
	
	public void setIndex(int _index)
	{
		index = _index;
	}
	
	public int getIndex()
	{
		return index;
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		if (g instanceof Graphics2D) {
			if (changeOpacity) {
				Graphics2D g2 = (Graphics2D)g;
				g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, opacity));
			}
		}
		g.drawImage(gameImage, 0, 0, gameImage.getWidth(), gameImage.getHeight(), this);
	}
}
