package makao.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.BorderFactory;

import makao.ImageResourceManager;

@SuppressWarnings("serial")
public class GameDynamicSymbol extends GameImage
{
	private String text;
	private int textWidth;
	private int textHeight;
	
	private Font font;
	private int points;
	
	public GameDynamicSymbol(int _symbolId, int _points)
	{
		super(ImageResourceManager.getResourceFilePath(_symbolId));
		text = Integer.toString(_points);
		font = new Font(Font.SANS_SERIF, Font.PLAIN, 50);
		points = _points;
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		g.setFont(font);
		textWidth = g.getFontMetrics().stringWidth(text);
		textHeight = g.getFontMetrics().getHeight();
		
		setSize(gameImage.getWidth(), gameImage.getHeight() + textHeight);
		setPreferredSize(new Dimension(gameImage.getWidth(), gameImage.getHeight()));
		
		g.drawImage(gameImage, 0, 0, gameImage.getWidth(), gameImage.getHeight(), this);
		int textX = (gameImage.getWidth() / 2) - (textWidth / 2);
		if (points > 0) {
			g.drawString(text, textX, gameImage.getHeight() + textHeight);
		}
	}
}
