package makao.gui;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import makao.ImageResourceManager;

@SuppressWarnings("serial")
public abstract class GameImage extends JPanel
{
	protected BufferedImage gameImage;
	
	public GameImage(String _imagePath)
	{
		try {
			if (ImageResourceManager.getInstance().isRunningFromJar()) {
				InputStream resourceInputStream = ImageResourceManager.getInstance().getResourceInputStream(_imagePath);
				gameImage = ImageIO.read(resourceInputStream);
			} else {
				String resourceFilePath = ImageResourceManager.getInstance().getResourcesEnvBasePath() + "\\" + _imagePath;
				gameImage = ImageIO.read(new File(resourceFilePath));
			}
			setBorder(null);
			setBackground(Color.black);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
