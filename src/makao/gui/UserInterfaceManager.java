package makao.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

import makao.ImageResourceManager;
import makao.logic.GameEngine;
import makao.logic.GameState;
import makao.model.card.Card;
import makao.model.card.CardColor;
import makao.model.card.CardCombo;
import makao.model.card.CardFigure;
import makao.model.player.Player;
import makao.model.player.PlayerQueue;
import makao.model.player.PlayerType;
import makao.model.player.cpu.CpuPlayerLevel;

public class UserInterfaceManager implements ActionListener, MouseListener {

	private JButton btn_addPlayer, btn_startGame, btn_endMove, btn_undoMove, btn_sayMakao, btn_sayAfterMakao;
	private JTextArea txtar_gameLog;
	private JList<String> lst_playerList;

	private JPanel panel_userCards, panel_boardCard, panel_pickUpCard, panel_userCardsCombo, panel_cpuCardsCombo, panel_gameColor, panel_gameRequest, panel_gameBattlePoints, panel_gameWaitQueues;
	private JProgressBar prgbar_cpuActionTimeout;
	
	private JFrame frm_mainGameWindow;
	private GameEngine gameEngine;
	
	private Random random = new Random();
	
	private int loggerCount = 0;

	public UserInterfaceManager(GameEngine _gameEngine, JFrame _mainWindow)
	{
		gameEngine = _gameEngine;
		frm_mainGameWindow = _mainWindow;
	}
	
	private DefaultListModel<String> getPlayerList()
	{
		return (DefaultListModel<String>)lst_playerList.getModel();
	}
	
	public void addGameLog(String _message)
	{
		String prefixLogger = "";
		if (!_message.isEmpty()) {
			prefixLogger += ++loggerCount + ". ";
		}
		txtar_gameLog.append(prefixLogger + _message + "\n");
		((DefaultCaret)txtar_gameLog.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
	}
	
	public void updateGameState(GameState _gameState)
	{
		int colorType = 0;
		CardColor boardColor = _gameState.getBoardCard().getColor();
		switch (boardColor) {
		case CARD_COLOR_CLUB:
			colorType = ImageResourceManager.IMG_RES_ID_GS_COLOR_CLUB;
			break;
			
		case CARD_COLOR_DIAMOND:
			colorType = ImageResourceManager.IMG_RES_ID_GS_COLOR_DIAMOND;
			break;
			
		case CARD_COLOR_HEART:
			colorType = ImageResourceManager.IMG_RES_ID_GS_COLOR_HEART;
			break;
			
		case CARD_COLOR_SPADE:
			colorType = ImageResourceManager.IMG_RES_ID_GS_COLOR_SPADE;
			break;
			
		default: case CARD_COLOR_NONE: break;
		}
		
		int figureType = 0;
		CardFigure reqFigure = _gameState.getJackRequest();
		if (reqFigure == null) {
			figureType = ImageResourceManager.IMG_RES_ID_GS_REQ_NONE;
		} else {
			switch (reqFigure) {			
			case CARD_FIGURE_5:
				figureType = ImageResourceManager.IMG_RES_ID_GS_REQ_5;
				break;
				
			case CARD_FIGURE_6:
				figureType = ImageResourceManager.IMG_RES_ID_GS_REQ_6;
				break;
				
			case CARD_FIGURE_7:
				figureType = ImageResourceManager.IMG_RES_ID_GS_REQ_7;
				break;
				
			case CARD_FIGURE_8:
				figureType = ImageResourceManager.IMG_RES_ID_GS_REQ_8;
				break;
				
			case CARD_FIGURE_9:
				figureType = ImageResourceManager.IMG_RES_ID_GS_REQ_9;
				break;
				
			case CARD_FIGURE_10:
				figureType = ImageResourceManager.IMG_RES_ID_GS_REQ_10;
				break;
				
			case CARD_FIGURE_KING:
				figureType = ImageResourceManager.IMG_RES_ID_GS_REQ_KING;
				break;
				
			case CARD_FIGURE_NONE:
				figureType = ImageResourceManager.IMG_RES_ID_GS_REQ_NONE;
				break;
				
			case CARD_FIGURE_QUEEN:
				figureType = ImageResourceManager.IMG_RES_ID_GS_REQ_QUEEN;
				break;
				
			default: break;
			}
		}

		
		int battleType = 0;
		if (_gameState.getBattleCardStake() > 0) {
			battleType = ImageResourceManager.IMG_RES_ID_GS_BATTLE_ACTIVE;
		} else {
			battleType = ImageResourceManager.IMG_RES_ID_GS_BATTLE_INACTIVE;
		}
		
		int waitQueueType = 0;
		if (_gameState.getQueues() > 0) {
			waitQueueType = ImageResourceManager.IMG_RES_ID_GS_QUEUE_ACTIVE;
		} else {
			waitQueueType = ImageResourceManager.IMG_RES_ID_GS_QUEUE_INACTIVE;
		}
		
		panel_gameColor.removeAll();
		panel_gameColor.add(new GameStaticSymbol(colorType));
		panel_gameColor.repaint();
		panel_gameColor.setVisible(true);
		
		panel_gameRequest.removeAll();
		panel_gameRequest.add(new GameStaticSymbol(figureType));
		panel_gameRequest.repaint();
		panel_gameRequest.setVisible(true);
		
		panel_gameBattlePoints.removeAll();
		panel_gameBattlePoints.add(new GameDynamicSymbol(battleType, _gameState.getBattleCardStake()));
		panel_gameBattlePoints.repaint();
		panel_gameBattlePoints.setVisible(true);
		
		panel_gameWaitQueues.removeAll();
		panel_gameWaitQueues.add(new GameDynamicSymbol(waitQueueType, _gameState.getQueues()));
		panel_gameWaitQueues.repaint();
		panel_gameWaitQueues.setVisible(true);
		
		frm_mainGameWindow.setVisible(true);
	}

	// Funkcja aktualizuje opis na liscie graczy (zaznacza * przy biezacym graczu)
	public void updateUIPlayerList(PlayerQueue _playerQueue)
	{
		ArrayList<Player> players = _playerQueue.getAllPlayers();
		int currentPlayer = _playerQueue.getCurrentPlayerPointer();
		getPlayerList().removeAllElements();
		for (int i = 0; i < players.size(); i++) {
			String playerDescStr = "";
			if (currentPlayer == i) {
				lst_playerList.setSelectedIndex(i);
				playerDescStr += "* ";
			}
			playerDescStr += players.get(i).toString();
			getPlayerList().addElement(playerDescStr);
			if (currentPlayer == i) {
				lst_playerList.setSelectedIndex(i);
			}
		}
	}

	public void updateUIUserCards(PlayerQueue _playerQueue)
	{
		ArrayList<Card> currentPlayerCards = _playerQueue.getCurrentPlayer().getAllCards();
		panel_userCards.removeAll();
		for (int i = 0; i < currentPlayerCards.size(); i++) {
			GameCardImage cardImage = new GameCardImage(currentPlayerCards.get(i), true, GameCardImageType.GAME_CARD_IMAGE_TYPE_SELECTABLE_CARD);
			//CardImage cardImage = new CardImage(currentPlayerCards.get(i), true, Color.DARK_GRAY);
			cardImage.setIndex(i);
			cardImage.addMouseListener(this);
			panel_userCards.add(cardImage);
		}
		panel_userCards.repaint();
		frm_mainGameWindow.setVisible(true);
	}

	public void updateUIUserComboCards(CardCombo _cardCombo)
	{
		panel_userCardsCombo.removeAll();

		for (int i = 0; i < _cardCombo.toList().size(); i++) {
			boolean isJoker = _cardCombo.toList().get(i).isJoker();
			//CardImage cardImage = new CardImage(_cardCombo.toList().get(i), isJoker, Color.black);
			GameCardImage cardImage = new GameCardImage(_cardCombo.toList().get(i), isJoker, GameCardImageType.GAME_CARD_IMAGE_TYPE_COMBO_CARD_USER);
			cardImage.addMouseListener(this);
			//cardImage.setInsideCardCombo();
			panel_userCardsCombo.add(cardImage);
		}

		panel_userCardsCombo.repaint();
		frm_mainGameWindow.setVisible(true);
	}
	
	public void updateUICpuComboCards(CardCombo _cardCombo)
	{
		/*panel_cpuCardsCombo.removeAll();
		
		for (int i = 0; i < _cardCombo.toList().size(); i++) {
			panel_cpuCardsCombo.add(new GameCardImage(_cardCombo.toList().get(i), false, GameCardImageType.GAME_CARD_IMAGE_TYPE_COMBO_CARD_CPU));
		}
		
		panel_cpuCardsCombo.repaint();
		frm_mainGameWindow.setVisible(true);*/
	}

	public void updateUIBoardCard(GameState _gameState)
	{
		// aktualizuj karte na stole
		panel_boardCard.removeAll();
		ArrayList<Card> boardHistory = _gameState.getBoardHistory();
		for (int i = 0; i < boardHistory.size(); i++) {
			if (i == (boardHistory.size() - 1)) {
				panel_boardCard.add(new GameCardImage(boardHistory.get(i), false, GameCardImageType.GAME_CARD_IMAGE_TYPE_BOARD_CARD));
			} else {
				panel_boardCard.add(new GameCardImage(boardHistory.get(i), false, GameCardImageType.GAME_CARD_IMAGE_TYPE_BOARD_CARD_HISTORY));
			}
		}
		frm_mainGameWindow.setVisible(true);
	}
	
	public void updateUIPickUpCard()
	{
		// zmienia kolor karty do brania na jakis inny
		panel_pickUpCard.removeAll();
		//CardImage pickUpCard = new CardImage(random.nextBoolean() ? Card.CARD_HIDDEN_BLUE : Card.CARD_HIDDEN_RED, true, Color.black);
		GameCardImage pickUpCard = new GameCardImage(random.nextBoolean() ? Card.CARD_HIDDEN_BLUE : Card.CARD_HIDDEN_RED, true, GameCardImageType.GAME_CARD_IMAGE_TYPE_PICKUP_CARD);
		pickUpCard.setIndex(-1);
		pickUpCard.addMouseListener(this);
		panel_pickUpCard.add(pickUpCard);
	}
	
	public void showUserPlainMessage(String _message)
	{
		JOptionPane.showMessageDialog(frm_mainGameWindow, _message, "Info", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void showUserError(String _message)
	{
		JOptionPane.showMessageDialog(frm_mainGameWindow, _message, "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public boolean askUserYesNo(String _message)
	{
		int result = JOptionPane.showConfirmDialog(frm_mainGameWindow, _message, "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		return result == JOptionPane.YES_OPTION;
	}
	
	public CardColor askUserForColor(int _optionsType)
	{
		String[] options = getAskUserOptions(_optionsType);
		String cardColor = (String) JOptionPane.showInputDialog(frm_mainGameWindow, "Please select card color", "Card color", JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
		if (cardColor == null) {
			return null;
		}
		
		return Card.getColorEnum(cardColor); 
	}
	
	public CardFigure askUserForFigure(int _optionsType)
	{
		String[] options = getAskUserOptions(_optionsType);
		String cardFigure = (String) JOptionPane.showInputDialog(frm_mainGameWindow, "Please select card figure", "Card figure", JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
		if (cardFigure == null) {
			return null;
		}
		
		return Card.getFigureEnum(cardFigure);
	}
	
	public PlayerType askUserForPlayerType()
	{
		String[] options = getAskUserOptions(ASK_USER_OPTIONS_PLAYER_TYPES);
		
		String playerType = (String) JOptionPane.showInputDialog(frm_mainGameWindow, "Please select player type", "Player type", JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
		if (playerType == null) {
			return null;
		}
		
		return Player.getPlayerTypeEnum(playerType);
	}
	
	public CpuPlayerLevel askUserForCpuLevel()
	{
		String[] options = getAskUserOptions(ASK_USER_OPTIONS_CPU_LEVEL_TYPE);
		
		String cpuPlayerLevel = (String) JOptionPane.showInputDialog(frm_mainGameWindow, "Please select cpu player level", "Computer level", JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
		if (cpuPlayerLevel == null) {
			return null;
		}
		
		return Player.getCpuPlayerLevelEnum(cpuPlayerLevel);
	}
	
	public String askUserForInputText(String _message)
	{
		return JOptionPane.showInputDialog(frm_mainGameWindow, _message, "", JOptionPane.PLAIN_MESSAGE);
	}
	
	public void setProgressBarValue(int _value)
	{
		prgbar_cpuActionTimeout.setValue(_value);
	}
	
	public int getProgressBarValue()
	{
		return prgbar_cpuActionTimeout.getValue();
	}

	public void setButtonUIComponents(
		JButton _btnAddPlayer,
		JButton _btnStartGame,
		JButton _btnEndMove,
		JButton _btnUndoMove,
		JButton _btnSayMakao,
		JButton _btnSayAfterMakao
	) {
		btn_addPlayer = _btnAddPlayer;
		btn_startGame = _btnStartGame;
		btn_endMove = _btnEndMove;
		btn_undoMove = _btnUndoMove;
		btn_sayMakao = _btnSayMakao;
		btn_sayAfterMakao = _btnSayAfterMakao;
	}

	public void setGameLogUIComponent(JTextArea _gameLog)
	{
		txtar_gameLog = _gameLog;
	}

	public void setPlayerListUIComponent(JList<String> _playerList)
	{
		lst_playerList = _playerList;
	}

	public void setPanelUIComponents(
		JPanel _userCardsPanel,
		JPanel _boardCard,
		JPanel _pickUpCard,
		JPanel _userCardsCombo,
		JPanel _cpuCardsComboPanel,
		JPanel _gameColorPanel,
		JPanel _gameRequestPanel,
		JPanel _gameBattlePointsPanel,
		JPanel _gameWaitQueuesPanel
	) {
		panel_userCards = _userCardsPanel;
		panel_boardCard = _boardCard;
		panel_pickUpCard = _pickUpCard;
		panel_userCardsCombo = _userCardsCombo;
		panel_cpuCardsCombo = _cpuCardsComboPanel;
		panel_gameColor = _gameColorPanel;
		panel_gameRequest = _gameRequestPanel;
		panel_gameBattlePoints = _gameBattlePointsPanel;
		panel_gameWaitQueues = _gameWaitQueuesPanel;
	}

	public void setProgressBarUIComponent(JProgressBar _progressBar)
	{
		prgbar_cpuActionTimeout = _progressBar;
	}
	
	@Override public void actionPerformed(ActionEvent e) {
		Object srcObject = e.getSource();  
		if (srcObject instanceof JButton) {
			if (srcObject == btn_addPlayer) {
				gameEngine.eventButton(ButtonEventID.BUTTON_EVENT_ID_ADD_PLAYER);
			} else if (srcObject == btn_startGame) {
				gameEngine.eventButton(ButtonEventID.BUTTON_EVENT_ID_START_GAME);
			} else if (srcObject == btn_endMove) {
				gameEngine.eventButton(ButtonEventID.BUTTON_EVENT_ID_END_MOVE);
			} else if (srcObject == btn_undoMove) {
				gameEngine.eventButton(ButtonEventID.BUTTON_EVENT_ID_UNDO_MOVE);
			} else if (srcObject == btn_sayMakao) {
				gameEngine.eventButton(ButtonEventID.BUTTON_EVENT_ID_SAY_MAKAO);
			} else if (srcObject == btn_sayAfterMakao) {
				gameEngine.eventButton(ButtonEventID.BUTTON_EVENT_ID_SAY_AFTER_MAKAO);
			}
		}
	}
	
	@Override public void mouseClicked(MouseEvent e) {
		Object srcObject = e.getSource();
		if (srcObject instanceof GameCardImage) {
			GameCardImage cardImage = (GameCardImage)srcObject;
			if (cardImage.getCardImageType() == GameCardImageType.GAME_CARD_IMAGE_TYPE_COMBO_CARD_USER) {
				gameEngine.eventMouse(MouseEventID.MOUSE_EVENT_ID_SELECT_COMBO_CARD, cardImage);
			} else if (cardImage.getCardImageType() == GameCardImageType.GAME_CARD_IMAGE_TYPE_PICKUP_CARD) {
				gameEngine.eventMouse(MouseEventID.MOUSE_EVENT_ID_PICKUP_CARD, cardImage);
			} else if (cardImage.getCardImageType() == GameCardImageType.GAME_CARD_IMAGE_TYPE_SELECTABLE_CARD) {
				gameEngine.eventMouse(MouseEventID.MOUSE_EVENT_ID_SELECT_USER_CARD, cardImage);
			}
		}
	}
	
	@Override public void mousePressed(MouseEvent e) { }
	@Override public void mouseReleased(MouseEvent e) { }
	@Override public void mouseEntered(MouseEvent e) { }
	@Override public void mouseExited(MouseEvent e) { }
	
	public static final int ASK_USER_OPTIONS_ALL_NON_JOKER_COLORS = 0;
	public static final int ASK_USER_OPTIONS_ALL_NON_SPECIAL_FIGURES_ONLY = 1;
	public static final int ASK_USER_OPTIONS_ALL_FIGURES_EXCEPT_JOKER = 2;
	public static final int ASK_USER_OPTIONS_PLAYER_TYPES = 3;
	public static final int ASK_USER_OPTIONS_CPU_LEVEL_TYPE = 4;
	
	public static String[] allNonJokerColors = new String[] {
		"Club", "Spade", "Diamond", "Heart",
	};
	
	public static String[] allNonSpecialFiguresOnly = new String[] {
		"5", "6", "7", "8", "9", "10", "King",
	};
	
	public static String[] allFiguresExceptJoker = new String[] {
		"2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace",
	};
	
	public static String[] playerTypes = new String[] {
		"User", "Cpu",
	};
	
	public static String[] cpuLevelTypes = new String[] {
		"VeryEasy", "Easy", "Medium", "Hard", "VeryHard",
	};
	
	public static String[] getAskUserOptions(int _optionsType)
	{
		String[] result = null;
		switch (_optionsType) {
		case ASK_USER_OPTIONS_ALL_NON_JOKER_COLORS:
			result = allNonJokerColors;
			break;
			
		case ASK_USER_OPTIONS_ALL_NON_SPECIAL_FIGURES_ONLY:
			result = allNonSpecialFiguresOnly;
			break;
			
		case ASK_USER_OPTIONS_ALL_FIGURES_EXCEPT_JOKER:
			result = allFiguresExceptJoker;
			break;
			
		case ASK_USER_OPTIONS_PLAYER_TYPES:
			result = playerTypes;
			break;
			
		case ASK_USER_OPTIONS_CPU_LEVEL_TYPE:
			result = cpuLevelTypes;
			break;
			
		default: result = null; break;
		}
		
		return result;
	}
}
