package makao.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.BorderFactory;

import makao.ImageResourceManager;

@SuppressWarnings("serial")
public class GameStaticSymbol extends GameImage
{
	public GameStaticSymbol(int _symbolId)
	{
		super(ImageResourceManager.getResourceFilePath(_symbolId));
		
		setSize(gameImage.getWidth(), gameImage.getHeight());
		setPreferredSize(new Dimension(gameImage.getWidth(), gameImage.getHeight()));
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(gameImage, 0, 0, gameImage.getWidth(), gameImage.getHeight(), this);
	}
}
