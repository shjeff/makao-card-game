package makao.logic;

import java.util.ArrayList;

import makao.model.card.Card;
import makao.model.card.CardCombo;
import makao.model.card.CardComparator;
import makao.model.card.CardCompareResult;
import makao.model.card.CardFigure;
import makao.model.player.PlayerQueue;

public class MoveValidator
{
	private GameState gameStatus;
	private PlayerQueue playerQueue;
	
	// klasa sprawdza czy mozna dokonac ruchu
	public MoveValidator()
	{
		
	}
	
	public void setGameStatus(GameState _gameStatus)
	{
		gameStatus = _gameStatus;
	}
	
	public void setPlayerQueue(PlayerQueue _playerQueue)
	{
		playerQueue = _playerQueue;
	}
	
	// gdzie to przeniesc?
	public boolean isPlayerFinishing(CardCombo _cardCombo)
	{
		ArrayList<Card> playerCards = playerQueue.getCurrentPlayer().getAllCards();
		if (playerCards.size() == 0) {
			// uzytkownik nie ma juz zadnych kart, kladzie cale combo kart walecznych
			if (_cardCombo.allCardsAreBattleWithPass()) {
				return true;
			}
		}
		return false;
	}
	
	public boolean validatePullingCard()
	{
		// czy gracz moze targac karte?
		return true;
	}
	
	public boolean checkCardCombo(CardCombo _cardCombo)
	{
		if (_cardCombo.containsJokers())
		{
			if (!_cardCombo.isJokersPreferenceFilled()) {
				// blad, nie wypelniono jokerow
				System.err.println("Nie wypelniono jokerow");
				return false;
			} else {
				// upewnij sie ze joki sa podmienione
				_cardCombo.replaceJokersWithPreference(); // jopki w combo?
			}
		}
		
		// status gry
		if (gameStatus.getGameState() == GameStateType.GAME_STATE_TYPE_NORMAL)
		{
			// zwykle dopasowanie pierwszej karty kombo pod kolor, drugiej i kolejnej pod figure
			if (CardComparator.compareCards(gameStatus.getBoardCard(), _cardCombo.first()) == CardCompareResult.COMPARE_RESULT_NONE) {
				System.err.println("[normal] pierwsza karta combo nie pasuje do stolu #1 [" + gameStatus.getBoardCard().toString() + "] [" + _cardCombo.first().toString() + "]");
				return false;
			}
			
			if (isPlayerFinishing(_cardCombo))
			{
				// gracz rzuca ostatnie karty i sa to karty waleczne. Nikt wczesniej nic nie rzucil
				if (CardComparator.compareCards(gameStatus.getBoardCard(), _cardCombo.first()) == CardCompareResult.COMPARE_RESULT_NONE) {
					System.err.println("[normal-player-ending] pierwsza karta combo nie pasuje do stolu #2 [" + gameStatus.getBoardCard().toString() + "] [" + _cardCombo.first().toString() + "]");
					return false;
				}
			}
			else
			{
				// nie mozna rzucic kart o roznych figurach to nie jest combo
				if (!_cardCombo.allCardsHaveSameFigure()) {
					System.err.println("[normal] rozne figury kart combo");
					return false;
				}
				
				// jezeli combo zawiera karty waleczne
				if (_cardCombo.containsBattleCards())
				{
					// jezeli nie wszystkie karty w combo sa waleczne np. krol n.w. krol w.
					if (!_cardCombo.allCardsAreBattle()) {
						// sprawdz czy jest zachowany porzadek krol n.w. krol n.w. krol n.w. krol W. krol W.
						// jezeli nie to blad
						if (!_cardCombo.isBattleCardsOrderValid()) {
							System.err.println("[normal] porzadek kart walecznych combo nieprawidlowy. [krol-waleczny, krol-niewaleczny]");
							return false;
						}
					}
				}
			}
			
			System.err.println("[normal] pass");
			return true;
		}
		else
		{
			// tutaj jest juz ustawiony jakis status gry
			
			if (gameStatus.getGameState() == GameStateType.GAME_STATE_TYPE_BATTLE_STAKE)
			{
				if (isPlayerFinishing(_cardCombo))
				{
					// gracz rzuca ostatnie karty i sa to karty waleczne, jest to odpowiedz na karty ktore ktos rzucil
					// musi pasowac pierwsza karta z combo
					if (CardComparator.compareCards(gameStatus.getBoardCard(), _cardCombo.first()) == CardCompareResult.COMPARE_RESULT_NONE) {
						System.err.println("[battle] pierwsza karta combo nie pasuje do stolu #1 [" + gameStatus.getBoardCard().toString() + "] [" + _cardCombo.first().toString() + "]");
						return false;
					}
				}
				else
				{
					if (!_cardCombo.allCardsAreBattle()) {
						// mozna klasc tylko karty waleczne na waleczne i zadne inne
						System.err.println("[battle] znaleziono karte niewaleczna");
						return false;
					}
					
					if (!_cardCombo.allCardsHaveSameFigure()) {
						// nie mozna mieszac i rzucac jednczesnie kart tego samego koloru i roznej figury, 2, 3, krol
						// co do kroli nie walecznych to nie mozna ich rzucic kiedy status gry jest taki ze juz ktos
						// rzucil waleczne. Nie mozna zadnych kart n.w. rzucic na w.
						// wiec tylko zostaje sprawdzenie figury tutaj a nie kolejnosci karty w. karty n.w.
						System.err.println("[battle] rozne figury kart combo");
						return false;
					}
					
					// pierwsza karta combo ma pasowac do tego co na stole
					if (CardComparator.compareCards(gameStatus.getBoardCard(), _cardCombo.first()) == CardCompareResult.COMPARE_RESULT_NONE) {
						System.err.println("[battle] pierwsza karta combo nie pasuje do stolu #2 [" + gameStatus.getBoardCard().toString() + "] [" + _cardCombo.first().toString() + "]");
						return false;
					}
				}
				
				System.err.println("[battle] pass");
				return true;
			}
			else if (gameStatus.getGameState() == GameStateType.GAME_STATE_TYPE_JACK_REQUEST)
			{
				
				if (!_cardCombo.allCardsHaveSameFigure()) {
					// tylko combo kart
					System.err.println("[jack-request] rozne figury kart combo");
					return false;
				}
				
				if (_cardCombo.containsBattleCards()) {
					// zadnych walecznych
					System.err.println("[jack-request] znaleziono karty waleczne");
					return false;
				}
				
				if (gameStatus.getBoardCard().isJack()) {
					// mozna podbic zadanie jopkiem lub polozyc zadana karte
					if (!_cardCombo.first().isJack()) {
						// inna karta na jopka musi pasowac pod figure
						if (gameStatus.getJackRequest() != _cardCombo.first().getFigure()) {
							System.err.println("[jack-request] JACK-ON-BOARD pierwsza karta w combo nie pasuje do zadania");
							return false;
						}
					}
				} else {
					if (gameStatus.getJackRequest() != _cardCombo.first().getFigure()) {
						System.err.println("[jack-request] CARD-ON-BOARD pierwsza karta w combo nie pasuje do zadania");
						return false;
					}
				}
				
				System.err.println("[jack-request] pass");
				return true;
			}
			else if (gameStatus.getGameState() == GameStateType.GAME_STATE_TYPE_QUEUE_REQUEST)
			{
				if (_cardCombo.first().getFigure() != CardFigure.CARD_FIGURE_4)
				{
					System.err.println("[wait-queue] karta w combo nie jest czworka");
					return false;
				}
				
				if (!_cardCombo.allCardsHaveSameFigure()) {
					System.err.println("[wait-queue] rozne figury kart combo");
					return false;
				}
				
				System.err.println("[wait-queue] pass");
				return true;
			}
		}
		
		return true;
	}
}
