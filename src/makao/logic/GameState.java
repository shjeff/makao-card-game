package makao.logic;

import java.util.ArrayList;

import makao.model.card.Card;
import makao.model.card.CardColor;
import makao.model.card.CardFigure;
import makao.model.player.Player;

public class GameState
{
	// karta na stole
	private Card boardCard;
	
	// zadanie jopka, jak jopek nic nie zada to jest null (dopasowanie pod kolor i figure jopka)
	// jak jopek zada jakiejs figury to to pole zawiera ktora figure chce jopek, wtedy jak na stole jest jeszcze jopek to zadanie mozna przebic
	// ale jak juz ktos polozyl zadana figure to nie mozna rzucic jopka. Jak juz ktos zacznie spelniac zadanie jopka to rzucaja wszyscy ta figure
	private CardFigure jackRequest;
	
	// kto rzucil jopka
	private Player jackRequestor;
	
	// ile kolejek jest na gracza
	private int numQueues;
	
	// ile kart jest do wziecia (karty waleczne)
	private int battleCardsStake;
	
	private GameStateType gameStateType;
	
	private ArrayList<Card> boardCardHistory = new ArrayList<Card>();
	
	public GameState()
	{
		boardCard = null;
		jackRequest = null;
		numQueues = 0;
		battleCardsStake = 0;
		gameStateType = GameStateType.GAME_STATE_TYPE_NORMAL;
	}
	
	public boolean isBackwardBattleKingOnBoard()
	{
		return (gameStateType == GameStateType.GAME_STATE_TYPE_BATTLE_STAKE) && (boardCard.getFigure() == CardFigure.CARD_FIGURE_KING) && (boardCard.getColor() == CardColor.CARD_COLOR_SPADE); 
	}
	
	private void addHistoryBoardCard(Card _card)
	{
		boardCardHistory.add(_card);
		if (boardCardHistory.size() == 11) {
			boardCardHistory.remove(0);
		}
	}
	
	public ArrayList<Card> getBoardHistory()
	{
		return boardCardHistory;
	}
	
	public void putBoardCard(Card _card)
	{
		boardCard = _card;
		addHistoryBoardCard(_card);
	}
	
	public Card getBoardCard()
	{
		return boardCard;
	}
	
	// jopki zadanie figury
	
	public void setJackRequestor(Player _jackRequestor)
	{
		jackRequestor = _jackRequestor;
	}
	
	public Player getJackRequestor()
	{
		return jackRequestor;
	}
	
	public void setJackRequest(CardFigure _jackRequest)
	{
		jackRequest = _jackRequest;
		if (jackRequest != null) {
			gameStateType = GameStateType.GAME_STATE_TYPE_JACK_REQUEST;
		}
	}
	
	public CardFigure getJackRequest()
	{
		return jackRequest;
	}
	
	public void clearJackRequest()
	{
		jackRequest = null;
		jackRequestor = null;
		gameStateType = GameStateType.GAME_STATE_TYPE_NORMAL;
	}
	
	// kolejki
	
	public void addQueues(int _numQueuesToAdd)
	{
		numQueues += _numQueuesToAdd;
		gameStateType = GameStateType.GAME_STATE_TYPE_QUEUE_REQUEST;
	}
	
	public void clearQueues()
	{
		numQueues = 0;
		gameStateType = GameStateType.GAME_STATE_TYPE_NORMAL;
	}
	
	public int getQueues()
	{
		return numQueues;
	}
	
	// waleczne
	
	public void addBattleCardStake(int _stakeNumber)
	{
		battleCardsStake += _stakeNumber;
		gameStateType = GameStateType.GAME_STATE_TYPE_BATTLE_STAKE;
	}
	
	public int getBattleCardStake()
	{
		return battleCardsStake;
	}
	
	public void clearBattleCardStake()
	{
		battleCardsStake = 0;
		gameStateType = GameStateType.GAME_STATE_TYPE_NORMAL;
	}
	
	public GameStateType getGameState()
	{
		return gameStateType;
	}
}
