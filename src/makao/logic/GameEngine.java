package makao.logic;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.ListModel;

import makao.gui.ButtonEventID;
import makao.gui.GameCardImage;
import makao.gui.GameEvent;
import makao.gui.MouseEventID;
import makao.gui.UserInterfaceManager;
import makao.model.card.Card;
import makao.model.card.CardColor;
import makao.model.card.CardCombo;
import makao.model.card.CardComparator;
import makao.model.card.CardCompareResult;
import makao.model.card.CardFigure;
import makao.model.card.CardInfiniteWaist;
import makao.model.card.CardStack;
import makao.model.player.Player;
import makao.model.player.PlayerQueue;
import makao.model.player.PlayerType;
import makao.model.player.cpu.CpuMoveProcessor;
import makao.model.player.cpu.CpuPlayerLevel;

public class GameEngine
{
	private PlayerQueue playerQueue = new PlayerQueue();
	
	private CardStack cardStack = new CardStack();
	
	private GameState gameState = new GameState();
	private MoveValidator moveValidator = new MoveValidator();
	
	private Timer tmr_cpuWait = null;
	private boolean isCpuPlayingNow = false;
	
	// todo: zmienic typ na CardCombo?
	private ArrayList<Card> arr_userCardCombo = new ArrayList<Card>();
	
	private boolean userHasPickedCardUp;
	
	// czy jest grana jakas gra w danym czasie
	// jak zostaje jeden gracz to gra jest zakonczona
	// ostatni kto ma karty to przegrywa
	private boolean isGamePlaying;
	
	private UserInterfaceManager ui;
	
	// sprawdza czy gracz skonczyl gre kladac wiecej niz jedna karte i zostalo mu zero kart
	private boolean playerFinishedWithManyCards;
	
	public GameEngine()
	{
		isGamePlaying = false;
		userHasPickedCardUp = false;
		playerFinishedWithManyCards = false;
		moveValidator.setGameStatus(gameState);
		moveValidator.setPlayerQueue(playerQueue);
	}
	
	private String getGameLogMessage(int _gameEvent, Object ...objects)
	{
		// funkcja domyslnie zmienia i parsuje parametry ktore sa ogolnie dostepne w klasie
		// ale moga byc parametry ktore moga byc obliczone w jakims ifie albo petli
		// wtedy dla takiego parametru odpowiedni %1 odpowiada kolejnemu argumentowi
		// funkcji
		String gameEventMessage = GameEvent.getMessage(_gameEvent);

		// biezacy gracz
		if (playerQueue.getAllPlayers().size() > 0) {
			gameEventMessage = gameEventMessage.replaceAll("%CP", playerQueue.getCurrentPlayer().getPlayerName());
		}
		
		// biezaca karta na stole
		if (gameState.getBoardCard() != null) {
			gameEventMessage = gameEventMessage.replaceAll("%CRD", gameState.getBoardCard().toString());
		}
		
		// ilosc kart walecznych na stole
		gameEventMessage = gameEventMessage.replaceAll("%GBP", gameState.getBattleCardStake() + "");
		
		for (int i = 0; i < objects.length; i++) {
			if (objects[i] instanceof String) {
				gameEventMessage = gameEventMessage.replace("%" + (i + 1), objects[i].toString());
			} else if (objects[i] instanceof Integer) {
				int number = ((Integer)objects[i]).intValue();
				gameEventMessage = gameEventMessage.replace("%" + (i + 1), Integer.toString(number));
			}
		}
		
		return gameEventMessage;
	}
	
	public void setIntermediateLevelUserInterfaceManager(UserInterfaceManager _uiManager)
	{
		ui = _uiManager;
	}
	
	private void punishPlayer(int _numCards)
	{
		for (int i = 0; i < _numCards; i++) {
			playerQueue.getCurrentPlayer().addCard(getNewCardFromStack());
		}
	}
	
	private Card getNewCardFromStack()
	{
		Card card = cardStack.getCard();
		
		if (card == null) {
			cardStack.newCardPack();
			card = cardStack.getCard();
			ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_ISSUED_NEW_CARD_STACK));
		}
		
		return card;
	}
	
	private void userClearMakaoStatusForMoreCards()
	{
		// funkcja sprawdza przy zdarzeniu EVENT_UNDO cofanie karty z combo
		// czy rzeczywiscie gracz ma mniej kart jedna lub w ogole
		// jezeli ma wiecej to funkcja czysci status makao i po makale
		// wywolane przy zdarzeniu EVENT_END_MOVE
		// funkcja aktualizuje status makao i po makale (czysci go jezeli uzytkownik
		// kliknal przycisk UNDO)
		if (playerQueue.getCurrentPlayer().getAllCards().size() == 1)
		{
			// kasuj po makale
			if (playerQueue.getCurrentPlayer().wasAfterMakaoRaised()) {
				playerQueue.getCurrentPlayer().clearAfterMakao();
				ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_CLEARED_AFTER_MAKAO));
			}
		}
		else if (playerQueue.getCurrentPlayer().getAllCards().size() > 1)
		{
			if (playerQueue.getCurrentPlayer().wasMakaoRaised()) {
				ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_CLEARED_MAKAO));
				playerQueue.getCurrentPlayer().clearMakao();
			}
			if (playerQueue.getCurrentPlayer().wasAfterMakaoRaised()) {
				ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_CLEARED_AFTER_MAKAO));
				playerQueue.getCurrentPlayer().clearAfterMakao();
			}
		}
	}
	
	private void acceptCardCombo(CardCombo _cardCombo)
	{
		// funkcja aktualizuje stan gry
		// umieszcza combo kart na stole i zmienia status gry i robi rozne rzeczy
		// takie jak pytanie o kolor lub figure kiedy sie kladzie jopka lub asa
		
		// pokaz jakie karty kladzie gracz
		ArrayList<Card> playerCardCombo = _cardCombo.toList();
		for (int i = 0; i < playerCardCombo.size(); i++) {
			ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_PUTS_CARD, playerCardCombo.get(i).toString()));
		}
		
		if (_cardCombo.last().isJack())
		{
			// gracz rzuca jopka
			CardFigure jackRequest = askPlayerForJackRequest();
			gameState.setJackRequest(jackRequest);
			if (jackRequest != null) {
				gameState.setJackRequestor(playerQueue.getCurrentPlayer());
			}
			
			ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_JACK_REQUEST, (jackRequest == null) ? "None (match by color)" : Card.getFigureDesc(jackRequest)));
		}
		else if (_cardCombo.last().isAce())
		{
			// gracz rzuca asa
			CardColor aceColor = askPlayerForAceRequest();
			if (aceColor != null) {
				// jezeli gracz zazadal zmiany koloru to zmien kolor karty na stole
				_cardCombo.last().setAceColor(aceColor);
			}
			
			ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_CHANGED_GAME_COLOR, (aceColor == null) ? "None (current color)" : Card.getColorDesc(aceColor)));
		}
		else if (_cardCombo.last().isPause())
		{
			// gracz rzuca 4ke/ki
			gameState.addQueues(_cardCombo.numQueues());
		}
		else if (_cardCombo.last().isFightCard())
		{
			// gracz rzuca karty waleczne
			gameState.addBattleCardStake(_cardCombo.battleCardsStake());
		}
		else
		{
			// roznosci tutaj
			if (gameState.getGameState() == GameStateType.GAME_STATE_TYPE_JACK_REQUEST)
			{
				if (gameState.getJackRequestor() == playerQueue.getCurrentPlayer()) {
					// gracz zadal jakiejs figury i teraz ja tutaj dal
					// zatem czysc zadanie figury, wrocimy do normalnosci
					gameState.clearJackRequest();
				}
			}
		}
		
		// aktualizuj stol kart
		for (int i = 0; i < _cardCombo.length(); i++) {
			gameState.putBoardCard(_cardCombo.toList().get(i));
		}
		
		// aktualizuj interfejs i mow co na gorze
		ui.updateUIBoardCard(gameState);
		
		ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_LAST_CARD_ON_THE_BOARD));
	}
	
	private void punishPlayerForNotSayingMakao()
	{
		if (playerQueue.getCurrentPlayer().getPlayerType() == PlayerType.PLAYER_TYPE_USER) {
			ui.showUserPlainMessage("You got 5 cards for not saying MAKAO");
		}
		
		punishPlayer(5);
		
		if (playerQueue.getCurrentPlayer().getPlayerType() == PlayerType.PLAYER_TYPE_USER) {
			ui.updateUIUserCards(playerQueue);
		}
		
		ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_GOT_PUNISH_MAKAO_WAS_NOT_TOLD));
	}
	
	private void punishPlayerForNotSayingAfterMakao()
	{
		if (playerQueue.getCurrentPlayer().getPlayerType() == PlayerType.PLAYER_TYPE_USER) {
			ui.showUserPlainMessage("You got 5 cards for not saying AFTER MAKAO");
		}
		
		punishPlayer(5);
		
		if (playerQueue.getCurrentPlayer().getPlayerType() == PlayerType.PLAYER_TYPE_USER) {
			ui.updateUIUserCards(playerQueue);
		}
		
		ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_GOT_PUNISH_AFTER_MAKAO_WAS_NOT_TOLD));
	}
	
	private void punishPlayerForRequestingInvalidFigure()
	{
		// 4 bo gracz juz ciagnal jedna karte, bo nie mial
		punishPlayer(4);
		
		if (playerQueue.getCurrentPlayer().getPlayerType() == PlayerType.PLAYER_TYPE_USER)
		{
			ui.updateUIUserCards(playerQueue);
		}
		
		ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_GOT_PUNISH_BAD_JACK_REQUEST));
	}
	
	private void punishPlayerForAddingMoreCards()
	{
		ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_GETS_CARDS, gameState.getBattleCardStake()));
		
		// -1 bo juz wzial jedna karte
		punishPlayer(gameState.getBattleCardStake() - 1);
		
		gameState.clearBattleCardStake();
	}
	
	private void punishPlayerForAddingQueues(int _addQueues)
	{
		ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_GOT_PUNISH_WAIT_QUEUES, _addQueues));
		
		// -1 bo juz ten kto rzucil juz mial kolejke
		playerQueue.getCurrentPlayer().addQueue(_addQueues-1);
		
		gameState.clearQueues();
	}
	
	private CardFigure askPlayerForJackRequest()
	{
		if (playerQueue.getCurrentPlayer().getPlayerType() == PlayerType.PLAYER_TYPE_USER)
		{
			return ui.askUserForFigure(UserInterfaceManager.ASK_USER_OPTIONS_ALL_NON_SPECIAL_FIGURES_ONLY);
		}
		else
		{
			CpuMoveProcessor cpuMoveProcessor = new CpuMoveProcessor(playerQueue.getCurrentPlayer(), gameState);
			return cpuMoveProcessor.askForJackFigure();
		}
	}
	
	private CardColor askPlayerForAceRequest()
	{
		if (playerQueue.getCurrentPlayer().getPlayerType() == PlayerType.PLAYER_TYPE_USER)
		{
			return ui.askUserForColor(UserInterfaceManager.ASK_USER_OPTIONS_ALL_NON_JOKER_COLORS);
		}
		else
		{
			CpuMoveProcessor cpuMoveProcessor = new CpuMoveProcessor(playerQueue.getCurrentPlayer(), gameState);
			return cpuMoveProcessor.askForAceColor();
		}
	}
	
	private void checkPlayerRound()
	{
		// funkcja sprawdza rozne rzeczy podczas zmiany gracza
		// np. czy zglosil makao, lub po makale albo czy ktos
		// wygral i gra jest zakonczona
		
		// sprawdz status makao i po makale
		if (playerQueue.getCurrentPlayer().getAllCards().size() == 0)
		{
			// gracz ma zero kart. Mogl rzucic naraz wszystkie karty i trzeba
			// sprawdzic czy powiedzial wtedy makao i po makale
			// lub mogl miec jedna karte i ja rzucic wtedy trzeba sprawdzic czy
			// powiedzial tylko po makale
			if (playerFinishedWithManyCards) {
				if (!playerQueue.getCurrentPlayer().wasMakaoRaised()) {
					ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_CHECK_MAKAO_WAS_NOT_TOLD));
					punishPlayerForNotSayingMakao();
				} else {
					ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_CHECK_MAKAO_WAS_TOLD));
				}
			}
			
			// jezeli uzytkownik konczyl gre kladac po jednej karcie to w ostatnim ruchu majac jedna karte
			// jak ja rzucil to musimy sprawdzic czy powiedzial po makale
			if (!playerQueue.getCurrentPlayer().wasAfterMakaoRaised()) {
				ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_CHECK_AFTER_MAKAO_WAS_NOT_TOLD));
				punishPlayerForNotSayingAfterMakao();
			} else {
				ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_CHECK_AFTER_MAKAO_WAS_TOLD));
			}
			
			// jezeli gracz ma dalej zero kart (czyli powiedzial makao i po makale to gracz wygral)
			if (playerQueue.getCurrentPlayer().getAllCards().size() == 0) {
				ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_WINS));
				playerQueue.getCurrentPlayer().setAsWinner();
			}
		}
		else if (playerQueue.getCurrentPlayer().getAllCards().size() == 1)
		{
			// sprawdz czy bylo powiedziane makao
			// sprawdz czy bylo powiedzane makao i po makale
			if (!playerQueue.getCurrentPlayer().wasMakaoRaised()) {
				ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_CHECK_MAKAO_WAS_NOT_TOLD));
				punishPlayerForNotSayingMakao();
			} else {
				ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_CHECK_MAKAO_WAS_TOLD));
			}
		}
		
		// jezeli jest jeden gracz a reszta wygrala to zakoncz gre
		if (playerQueue.weHaveLooser()) {
			isGamePlaying = false;
			// kto przegral?
		}
		
		// jezeli gracz juz zglosil makao i dal karte i zostala mu jedna
		// to jak to przeszlo przez EndMove() to bylo to ok, ale teraz jak ma jedna
		// karte to trzeba mu to skasowac, inaczej:
		// skasuj status makao jak gracz juz powiedzial makao aby mial okazje znowu powiedziec
		// w swojej kolejce
		//
		// Nie. powinno byc tak ze gracz zglasza makao i po makale i ten status jest widoczny w liscie
		// graczy po lewym gornym rogu okienka GUI. Nastepnie jak przychodzi kolejka na gracza ktory zglosil
		// te statusy to wtedy dopiero moze byc skasowana, w tym wypadku tylko status MAKAO
	}
	
	private void checkBattleCardPunish()
	{
		if (gameState.getGameState() == GameStateType.GAME_STATE_TYPE_BATTLE_STAKE)
		{
			// gracz nic nie rzucil i zakonczyl ruch i wzial jedna karte jak mial waleczne
			// jak gracz daje karte waleczna to zawsze zmienia stol
			// wiec tutaj jak nic nie zostalo zmienione to daj kare
			
			punishPlayerForAddingMoreCards();
			
			if (playerQueue.getCurrentPlayer().getPlayerType() == PlayerType.PLAYER_TYPE_USER) {
				ui.updateUIUserCards(playerQueue);
			}
		}
	}
	
	private void checkJackRequestPunish(boolean _boardChanged)
	{
		// tylko jezeli gracz polozyl karte to:
		//   1. mogl zadac jopka
		//   2. polozyc to czego zadal
		// jezeli jest zadanie i gracz nie polozyl karty i jezeli
		// to jest wlasciciel zadania to wtedy nalezy skasowac status
		// i ukarac
		if (gameState.getGameState() == GameStateType.GAME_STATE_TYPE_JACK_REQUEST)
		{
			Player jackRequestor = gameState.getJackRequestor();
			
			if (!_boardChanged) {
				// gracz nie polozyl zadnej figury
				if (playerQueue.getCurrentPlayer().equals(jackRequestor)) {
					// kara za zadanie tego czego sie nie ma
					punishPlayerForRequestingInvalidFigure();
					
					// kasuj requestora i zadanie, kolejka jopkiem przeszla
					gameState.clearJackRequest();
				}
			} else {
				// gracz zadal jakiejs figury
				// gracz dal to czego zadal
				if (playerQueue.getCurrentPlayer().equals(jackRequestor)) {
					if (gameState.getBoardCard().getFigure() == gameState.getJackRequest()) {
						// tutaj gracz kladl to czego wczesniej zadal
						gameState.clearJackRequest();
					}
				}
			}
		}
	}
	
	private void checkQueues()
	{
		if (gameState.getGameState() == GameStateType.GAME_STATE_TYPE_QUEUE_REQUEST)
		{
			// gracz nie mial kolejek wiec trzeba mu dac kolejki
			punishPlayerForAddingQueues(gameState.getQueues());
		}
	}
	
	private void printGameState()
	{
		// wyswietl ile jest kart walecznych na gracza
		if (gameState.getGameState() == GameStateType.GAME_STATE_TYPE_BATTLE_STAKE) {
			ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_BATTLE_STATE_POINTS));
		}
		
		// wyswietl jaka figura jest zadana
		if (gameState.getGameState() == GameStateType.GAME_STATE_TYPE_JACK_REQUEST) {
			String jackRequest = (gameState.getJackRequest() == null) ? "None (match by color)" : Card.getFigureDesc(gameState.getJackRequest());
			ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_CURRENT_REQUEST, jackRequest));
		}
		
		// wyswietl biezaca ilosc kolejek
		if (gameState.getGameState() == GameStateType.GAME_STATE_TYPE_QUEUE_REQUEST) {
			ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_CURRENT_QUEUES, gameState.getQueues()));
		}
	}
	
	private void playerEndMove(boolean _boardChanged)
	{
		// koniec rundy gracza, jezeli gracz podbije jakies zadanie
		// albo zmieni karte na stole to jest OK, nie ma zadnych
		// kar ale jezeli nie zmieni to sprawdz czego nie mogl dac
		
		if (!_boardChanged) {
			// sprawdz branie kart (karty waleczne)
			checkBattleCardPunish();
			
			// sprawdz kolejki
			checkQueues();
		}
		
		// sprawdz czy gracz zada tego czego nie ma
		// ...lub kasuj zadanie jak to dal
		checkJackRequestPunish(_boardChanged);

		// sprawdz status makao i po makale i czy gracz wygral
		checkPlayerRound();
		
		// pisz status gry jak jakis jest
		printGameState();
		
		// informuj ze gracz konczy ruch
		ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_END_PLAYER_ROUND));
		
		// zakoncz gre jak zostal tylko jeden sam gracz
		if (!isGamePlaying) {
			ui.updateUIBoardCard(gameState);
			ui.updateUIPlayerList(playerQueue);
			ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_FINISHED));
			isCpuPlayingNow = false;
			return;
		}
		
		// nowa linia
		ui.addGameLog(GameEvent.GAME_EVENT_SPLIT);
		
		// przelacz na nastepnego gracza
		nextPlayer();
	}
	
	// Funkcja przelacza nastepnego gracza
	private void nextPlayer()
	{
		// pokaz ze nastepuje zmiana gracza
		ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_CHANGE));
		
		// aktualizuj status gracza (jak dal np. makao albo wygral)
		ui.updateUIPlayerList(playerQueue);
		
		// zmien gracza
		// jezeli karta waleczna i krol czarne serce to poprzedni gracz
		
		if (gameState.isBackwardBattleKingOnBoard()) {
			// karta waleczna na stole krol czarne serce, poprzedni gracz musi sie wybronic
			// jezeli poprzedni gracz ma kolejke to skocz do jeszcze poprzedniego gracza
			// ale nie zaliczaj mu przejscia kolejki (poprzedniemu graczowi ktory ma kolejke)
			playerQueue.previousPlayer();
			
			// pokaz ze poprzedni gracz ma krola czarne serce
			ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_BATTLE_FOR_PREVIOUS_PLAYER));
		} else {
			// jezeli jest karta waleczna na "do przodu" albo jakikolwiek inny status daj nastepnego gracza
			playerQueue.nextPlayer();
		}
		
		while (!playerQueue.getCurrentPlayer().isPlaying()) {
			
			// jezeli ma kolejke to zdejmij mu jedno przejscie i wrzuc to na log
			if (playerQueue.getCurrentPlayer().isWaiting()) {
				ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_WAITING_QUEUE, playerQueue.getCurrentPlayer().getPlayerName(), playerQueue.getCurrentPlayer().getQueues()));
				playerQueue.getCurrentPlayer().oneQueuePass();
			} else if (playerQueue.getCurrentPlayer().isWinner()) {
				ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_WAITING_WINNER, playerQueue.getCurrentPlayer().getPlayerName()));
			}
			
			if (gameState.isBackwardBattleKingOnBoard()) {
				// karta waleczna na stole krol czarne serce, poprzedni gracz musi sie wybronic
				// jezeli poprzedni gracz ma kolejke to skocz do jeszcze poprzedniego gracza
				// ale nie zaliczaj mu przejscia kolejki (poprzedniemu graczowi ktory ma kolejke)
				playerQueue.previousPlayer();
				
				// pokaz ze poprzedni gracz ma krola czarne serce
				ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_BATTLE_FOR_PREVIOUS_PLAYER));
			} else {
				// jezeli jest karta waleczna na "do przodu" albo jakikolwiek inny status daj nastepnego gracza
				playerQueue.nextPlayer();
			}
		}
		
		// nowa linia
		ui.addGameLog(GameEvent.GAME_EVENT_SPLIT);
		
		processPlayerMove();
	}
	
	private void cpuPlayerAction()
	{
		// tutaj wyskakuje jakis exception dla komputera ze niby w timerze cos robi
		// i aktualizuje i to trzeba cos wywolac przez .invokeLater(new Runnable(){...
		
		// tutaj mysli komputer...
		ui.updateUIBoardCard(gameState);
		
		// czy stol zostal zmieniony?
		boolean boardChanged = false;
		
		// kasuj makao i po makale
		if (playerQueue.getCurrentPlayer().wasMakaoRaised()) {
			playerQueue.getCurrentPlayer().clearMakao();
		}
		
		if (playerQueue.getCurrentPlayer().wasAfterMakaoRaised()) {
			playerQueue.getCurrentPlayer().clearAfterMakao();
		}
		
		CpuMoveProcessor cpuMoveProcessor = new CpuMoveProcessor(playerQueue.getCurrentPlayer(), gameState);
		CardCombo cpuCardCombo = cpuMoveProcessor.getCardCombo();
		if (cpuCardCombo.length() == 0)
		{
			// komputer nie ma kart ktore moze dac na stol
			if (gameState.getGameState() == GameStateType.GAME_STATE_TYPE_QUEUE_REQUEST)
			{
				// jezeli ktos rzucil na kompter kolejke ten nie moze ciagnac kart
				// musi sie poddac i przyjac kolejke
				boardChanged = false;
			}
			else
			{
				// komputer ciagnie karte
				ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_TAKES_ONE_CARD));
				Card cardFromStack = getNewCardFromStack();
				
				// dodaj karte ktora wzial komputer do jego talii
				playerQueue.getCurrentPlayer().addCard(cardFromStack);
				
				// sprawdz jeszcze raz czy mozna dac karte
				cpuCardCombo = cpuMoveProcessor.getCardCombo();
				
				if (cpuCardCombo.length() == 0)
				{
					// kicha jednak nie mozna nic dac, nic nie pasuje
					// komputerowi, zakoncz ruch
					boardChanged = false;
				} else {
					// komputer moze cos dac w koncu
					acceptCardCombo(cpuCardCombo);
					boardChanged = true;
				}
			}
		} else {
			// no prosze, komputer odrazu moze dac karte
			acceptCardCombo(cpuCardCombo);
			boardChanged = true;
		}
		
		playerFinishedWithManyCards = (playerQueue.getCurrentPlayer().getAllCards().size() == 0) && (cpuCardCombo.length() > 1);
		
		if (cpuCardCombo.length() > 0) {
			// podnies makao lub po makale dla komputera
			if (playerQueue.getCurrentPlayer().getAllCards().size() == 0) {
				if (cpuCardCombo.length() > 1) {
					playerQueue.getCurrentPlayer().raiseMakao();
					ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_SAYS_MAKAO));
					playerQueue.getCurrentPlayer().raiseAfterMakao();
					ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_SAYS_AFTER_MAKAO));
				} else if (cpuCardCombo.length() == 1) {
					// jedna karta zostala dodana
					playerQueue.getCurrentPlayer().raiseAfterMakao();
					ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_SAYS_AFTER_MAKAO));
				}
			} else if (playerQueue.getCurrentPlayer().getAllCards().size() == 1) {
				// zostaje jedna karta
				playerQueue.getCurrentPlayer().raiseMakao();
				ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_SAYS_MAKAO));
			}
		}
		
		// aktualizuj ikonki gry
		ui.updateGameState(gameState);
		
		// timer na ZAKONCZENIE ruchu, nie przed rozpoczeciem
		cpuInvokeWaitTimer(boardChanged);
	}
	
	private void cpuInvokeWaitTimer(boolean _boardChanged)
	{
		tmr_cpuWait = new Timer();
		tmr_cpuWait.schedule(new TimerTask() {
			@Override
			public void run() {
				int cpuProgBarValue = ui.getProgressBarValue();
				if (cpuProgBarValue < 100) {
					ui.setProgressBarValue(cpuProgBarValue + 1);
				} else {
					ui.setProgressBarValue(0);
					tmr_cpuWait.cancel();
					// zmien gracza, komputer skonczyl ruch
					playerEndMove(_boardChanged);
				}
			}
		}, 10, 10);
	}
	
	private void userInvokeTimer(boolean _boardChanged)
	{
		tmr_cpuWait = new Timer();
		tmr_cpuWait.schedule(new TimerTask() {
			@Override
			public void run() {
				int cpuProgBarValue = ui.getProgressBarValue();
				if (cpuProgBarValue < 100) {
					ui.setProgressBarValue(cpuProgBarValue + 1);
				} else {
					ui.setProgressBarValue(0);
					tmr_cpuWait.cancel();
					playerEndMove(_boardChanged);
				}
			}
		}, 10, 10);
	}
	
	private void processPlayerMove()
	{
		// poczatek ruchu gracza
		ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_ROUND));
		
		// resetuj wszystkie flagi dla wszystkich rodzajow graczy
		if (playerQueue.getCurrentPlayer().wasMakaoRaised()) {
			ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_CLEARED_MAKAO));
			playerQueue.getCurrentPlayer().clearMakao();
			playerQueue.getCurrentPlayer().clearAfterMakao();
		}
		
		ui.updateUIPlayerList(playerQueue);
		
		if (playerQueue.getCurrentPlayer().getPlayerType() == PlayerType.PLAYER_TYPE_CPU) {
			// komputer robi ruch
			
			// wywolaj timer od komputera
			isCpuPlayingNow = true;
			cpuPlayerAction();
		} else {
			// uzytkownik robi ruch
			ui.updateUIUserComboCards(new CardCombo(new ArrayList<Card>()));
			ui.updateUICpuComboCards(new CardCombo(new ArrayList<Card>()));
			
			// aktualizacja kart uzytkownika
			ui.updateUIUserCards(playerQueue);
			
			// aktualizuj ikonki gry
			ui.updateGameState(gameState);
			
			// resetuj jakiekolwiek flagi kiedy gra gracz
			isCpuPlayingNow = false;
			userHasPickedCardUp = false;
			playerFinishedWithManyCards = false;
		}
	}
	
	// Funkcje od gracza
	public void userEventEndMove()
	{
		// tutaj gracz konczy ruch. Jezeli to jest zwykly status gry to
		// wystarczy sprawdzic tylko combo kart. Jezeli status gry jest inny trzeba
		// sprawdzic inne rzeczy jak to czy gracz wzial karty ze stosu itp.
		CardCombo userCardCombo = new CardCombo(arr_userCardCombo);
		
		// funkcja czysci status makao i po makale jezeli gracz ma wiecej niz jedna karte
		// kliknal np. undo i dal karty tak ze mu zostaly dwie ale wczesniej kliknal makao i po makale
		userClearMakaoStatusForMoreCards();
		
		if (userCardCombo.length() == 0)
		{
			// gracz nie ma nic w combo
			// sprawdz czy bral jedna karte
			if (userHasPickedCardUp)
			{
				// jak bral to mozna "prawie" zakonczyc ruch
				playerEndMove(false);
			}
			else if (gameState.getGameState() == GameStateType.GAME_STATE_TYPE_QUEUE_REQUEST)
			{
				// tutaj mogl nie wziac karty jak nie mial czworek
				// wiec tez mozna zakonczyc ruch
				playerEndMove(false);
			}
			else
			{
				// a tutaj kliknal koniec ruchu, a nie ciagnal
				// karty ani nic nie dal w combo
				ui.showUserError("You must pull card from stack if you have not any cards");
			}
		} else {
			
			if (moveValidator.checkCardCombo(userCardCombo))
			{
				// jezeli pasuje combo kart, to mozna je polozyc i aktualizowac
				// stan gry
				acceptCardCombo(userCardCombo);
				int cardComboSize = userCardCombo.length();
				
				// Kasuj combo kart
				arr_userCardCombo.clear();
				
				// aktualizuj pole kart combo
				ui.updateUIUserComboCards(new CardCombo(arr_userCardCombo));
				
				// aktualizuj stol (poloz ostatnia karte)
				ui.updateUIBoardCard(gameState);
				
				// czy gracz konczy gre w jednym combo kladac dwie karty naraz?
				playerFinishedWithManyCards = (playerQueue.getCurrentPlayer().getAllCards().size() == 0) && (cardComboSize > 1);
				
				// koncz ruch, sprawdz jakiejs mniejsze rzeczy, np. makao i po makale
				playerEndMove(true);
			} else {
				ui.showUserError("You cannot put those cards in such order");
				
				// wroc wszystkie karty z powrotem do pulu kart uzytkownika
				while (arr_userCardCombo.size() > 0) {
					playerQueue.getCurrentPlayer().addCard(arr_userCardCombo.remove(arr_userCardCombo.size() - 1));
				}
				
				// aktualizuj miedzymordzie
				ui.updateUIUserCards(playerQueue);
				ui.updateUIUserComboCards(new CardCombo(new ArrayList<Card>()));
			}
		}
	}
	
	public void userEventPutCard(Card card, int index)
	{
		// uzytkownik wybiera karte do kladzenia
		// kasuj karte z kart biezacego gracza
		Card selCard = playerQueue.getCurrentPlayer().selectCardAtIndex(index);
		//if (!selCard.equals(card)) {
			//return;
		//}
		
		arr_userCardCombo.add(selCard);
		
		// aktualizuj panel kart combo
		ui.updateUIUserComboCards(new CardCombo(arr_userCardCombo));
		
		// odswiez karty uzytkownika
		ui.updateUIUserCards(playerQueue);
	}
	
	public void userEventPullCard()
	{
		GameStateType gameActionStatus = gameState.getGameState();
		
		if (gameActionStatus != GameStateType.GAME_STATE_TYPE_QUEUE_REQUEST)
		{
			// gracz bieze jedna karte
			// tego ruchu nie mozna cofnac
			playerQueue.getCurrentPlayer().addCard(getNewCardFromStack());
			userHasPickedCardUp = true;
			ui.updateUIUserCards(playerQueue);
			ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_TAKES_ONE_CARD));
		}
		else
		{
			// jak ktos rzucil kolejki to zablokuj, nie mozna
			// ciagnac jak ktos rzucil kolejki
			ui.showUserError("You can't pull card if you got queue");
		}
	}
	
	private void userEventUndoMove()
	{
		if (arr_userCardCombo.size() > 0) {
			// wroc karte wylozona jako combo do kart uzytkownika
			playerQueue.getCurrentPlayer().addCard(arr_userCardCombo.remove(arr_userCardCombo.size() - 1));
			ui.updateUIUserCards(playerQueue);
		}
		
		// aktualizuj pole kart combo
		ui.updateUIUserComboCards(new CardCombo(arr_userCardCombo));
		
		// jezeli gracz ma wiecej niz jedna karte to czysc status makao
		userClearMakaoStatusForMoreCards();
	}
	
	private ArrayList<Card> getSpecialPlayerCards(String[] strCards)
	{
		ArrayList<Card> cards = new ArrayList<Card>();
		
		for (int i = 0; i < strCards.length; i++) {
			String strCard = strCards[i];
			if (strCard.equals("Random")) {
				
				cards.add(getNewCardFromStack());
				
			} else {
				String[] strCardSplit = strCard.split(":");
				String strColor = strCardSplit[0];
				String strFigure = strCardSplit[1];
				
				cards.add(Card.getCardFromParams(Card.getColorEnum(strColor), Card.getFigureEnum(strFigure)));
			}
		}
		
		return cards;
	}
	
	public void userEventStartGame()
	{
		//playerQueue.addPlayer(Player.createUserPlayer("Konrad"));
		//playerQueue.addPlayer(Player.createCpuPlayer("Komputer #1", CpuPlayerLevel.CPU_PLAYER_LEVEL_EASY));
		//playerQueue.addPlayer(Player.createCpuPlayer("Komputer #2", CpuPlayerLevel.CPU_PLAYER_LEVEL_EASY));
		
		if (playerQueue.getAllPlayers().size() < 2) {
			ui.showUserError("You must add at least two players to the game");
			return;
		}
		
		ui.updateUIPlayerList(playerQueue);
		
		ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_START_GAME));
		
		// normal, jack, ace, queue, queen, battle, joker
		//cardInfiniteWaist.setChance(0.5, 0, 0, 0, 0, 0.5, 0);
		
		// testy
//		ArrayList<Card> userCards = getSpecialPlayerCards(new String[] {
//			"Club:5", "Club:6", "Club:7", "Diamond:9", "Diamond:9",
//		});
//		
//		ArrayList<Card> cpuCards1 = getSpecialPlayerCards(new String[] {
//			"Club:King", "Diamond:9", "Heart:6", "Spade:5", "Heart:Queen", "Club:10",
//		});
		
		/*ArrayList<Card> cpuCards2 = getSpecialPlayerCards(new String[] {
			"Club:5", "Club:6", "Club:7", "Diamond:9", "Diamond:9",
		});*/ 
		
//		for (Card card : userCards) {
//			playerQueue.getAllPlayers().get(0).addCard(card);
//		}
//		
//		for (Card card : cpuCards1) {
//			playerQueue.getAllPlayers().get(1).addCard(card);
//		}
		
		/*for (Card card : cpuCards2) {
			playerQueue.getAllPlayers().get(2).addCard(card);
		}*/
		
		
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < playerQueue.getAllPlayers().size(); j++) {
				Card card = getNewCardFromStack();
				playerQueue.getAllPlayers().get(j).addCard(card);
			}
		}
		
		// poloz na stol talie i losuj karte nie specjalna
		ui.updateUIPickUpCard();
		
		Card boardCard = getNewCardFromStack();
		
		while (boardCard.isSpecial()) {
			boardCard = getNewCardFromStack();
		}
		
		// poloz karte poczatkowa na stol !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		gameState.putBoardCard(boardCard);
		
		// aktualizuj interfejs GUI
		ui.updateUIBoardCard(gameState);
		
		isGamePlaying = true;
		
		processPlayerMove();
	}
	
	private void userEventAddPlayer()
	{
		String playerName;
		PlayerType playerType = null;
		CpuPlayerLevel cpuPlayerLevel = null;
		
		do {
			playerName = ui.askUserForInputText("Enter player name");
			if (playerName == null) {
				return;
			}
		} while (playerName == null || playerName.isEmpty());
		
		do {
			playerType = ui.askUserForPlayerType();
			if (playerType == null) {
				return;
			}
		} while (playerType == null);
		
		if (playerType == PlayerType.PLAYER_TYPE_CPU) {
			do {
				cpuPlayerLevel = ui.askUserForCpuLevel();
				if (cpuPlayerLevel == null) {
					return;
				}
			} while (cpuPlayerLevel == null);
			
			playerQueue.addPlayer(Player.createCpuPlayer(playerName, cpuPlayerLevel));
		} else {
			playerQueue.addPlayer(Player.createUserPlayer(playerName));
		}
		
		ui.updateUIPlayerList(playerQueue);
	}
	
	private void userEventSetMakaoStatus()
	{
		if (playerQueue.getCurrentPlayer().getAllCards().size() == 1) {
			if (!playerQueue.getCurrentPlayer().wasMakaoRaised()) {
				playerQueue.getCurrentPlayer().raiseMakao();
				ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_SAYS_MAKAO));
			}
		} else {
			ui.showUserError("You can raise MAKAO status only if you have one card");
		}
	}
	
	private void userEventSetAfterMakaoStatus()
	{
		if (playerQueue.getCurrentPlayer().getAllCards().size() == 0) {
			if (!playerQueue.getCurrentPlayer().wasAfterMakaoRaised()) {
				ui.addGameLog(getGameLogMessage(GameEvent.GAME_EVENT_PLAYER_SAYS_AFTER_MAKAO));
				playerQueue.getCurrentPlayer().raiseAfterMakao();
			}
		} else {
			ui.showUserError("You can raise AFTER MAKAO status if you don't have any cards");
		}
	}
	
	public void eventButton(ButtonEventID _buttonEventType)
	{
		if (isCpuPlayingNow) {
			return;
		}
		
		switch (_buttonEventType) {
		case BUTTON_EVENT_ID_ADD_PLAYER:
			userEventAddPlayer();
			break;
			
		case BUTTON_EVENT_ID_START_GAME:
			userEventStartGame();
			break;
			
		case BUTTON_EVENT_ID_END_MOVE:
			userEventEndMove();
			break;
			
		case BUTTON_EVENT_ID_UNDO_MOVE:
			userEventUndoMove();
			break;
			
		case BUTTON_EVENT_ID_SAY_MAKAO:
			userEventSetMakaoStatus();
			break;
			
		case BUTTON_EVENT_ID_SAY_AFTER_MAKAO:
			userEventSetAfterMakaoStatus();
			break;
			
		default:
			break;
		}
	}
	
	public void eventMouse(MouseEventID _mouseEventType, GameCardImage _gameCardImage)
	{
		if (isCpuPlayingNow) {
			ui.showUserError("Now CPU is playing, please wait for your turn");
			return;
		}
		
		switch (_mouseEventType) {
		case MOUSE_EVENT_ID_PICKUP_CARD:
			// moze byc roznie np. jak gracz ciagnie waleczne i pobral jedna karte to tutaj
			// po kliknieciu znowu ma mu wpasc reszta kart z walecznych 
			if (!userHasPickedCardUp) {
				ui.updateUIPickUpCard();
				userEventPullCard();
			}
			break;
			
		case MOUSE_EVENT_ID_SELECT_COMBO_CARD:
			Card card = _gameCardImage.getCard();
			if (card.isJoker() || card.getJokerPreference() != null) {
				if (_gameCardImage.getCard().getJokerPreference() == null) {
					
					CardColor colorPreference = ui.askUserForColor(UserInterfaceManager.ASK_USER_OPTIONS_ALL_NON_JOKER_COLORS);
					CardFigure figurePreference = ui.askUserForFigure(UserInterfaceManager.ASK_USER_OPTIONS_ALL_FIGURES_EXCEPT_JOKER);
					
					if (colorPreference == null || figurePreference == null) {
						return;
					}
					_gameCardImage.getCard().setJokerPreference(Card.getCardFromParams(colorPreference, figurePreference));
					_gameCardImage.getCard().exchange();
					ui.updateUIUserComboCards(new CardCombo(arr_userCardCombo));
				} else {
					_gameCardImage.getCard().exchange();
					_gameCardImage.getCard().clearJokerPreference();
				}
				
				ui.updateUIUserComboCards(new CardCombo(arr_userCardCombo));
			}
			break;
			
		case MOUSE_EVENT_ID_SELECT_USER_CARD:
			userEventPutCard(_gameCardImage.getCard(), _gameCardImage.getIndex());
			break;
			
		default:
			break;
		}
	}
}
