package makao;

import java.net.URL;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import makao.gui.GameWindow;
import makao.gui.UserInterfaceManager;
import makao.logic.GameEngine;

public class MakaoCardGame
{
	public static void main(String[] args)
	{
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run()
			{
				loadGame();
			};
		});
	}
	
	private boolean checkIfRunningInJar()
	{
		return MakaoCardGame.class.getResource("MakaoCardGame.class").toString().startsWith("jar:");
	}
	
	private MakaoCardGame()
	{
		ImageResourceManager.Initialize(getClass(), "C:\\Users\\jffs\\Documents\\workspaces\\eclipse\\MakaoCardGame\\jar\\res", checkIfRunningInJar());
		
		GameWindow gameWindow = new GameWindow();
		GameEngine gameEngine = new GameEngine();
		
		// zbinduj posrednika interfejsu UI z silnikiem gry
		UserInterfaceManager uiManager = new UserInterfaceManager(gameEngine, gameWindow);
		gameEngine.setIntermediateLevelUserInterfaceManager(uiManager);
		
		gameWindow.setGUIComponents(uiManager);
		gameWindow.setVisible(true);
	}
	
	private static void loadGame()
	{
		new MakaoCardGame();
	}
}
